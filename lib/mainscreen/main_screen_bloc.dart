import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:senme/dto/category.dart';
import 'package:senme/mainscreen/main_screen_event.dart';
import 'package:senme/mainscreen/main_screen_state.dart';

class MainScreenBloc extends Bloc<MainScreenEvent, MainScreenState> {
  final List<Category> categories;
  MainScreenBloc(MainScreenState initialState, this.categories)
      : super(initialState);

  @override
  Stream<MainScreenState> mapEventToState(MainScreenEvent event) async* {
    if (event is PageChanged) {
      print('event.isShowAppBar: ${event.isShowAppBar}');
      yield MainScreenStatePageChanged(
          event.selectedPage, event.title, event.isShowAppBar);
    }
  }
}
