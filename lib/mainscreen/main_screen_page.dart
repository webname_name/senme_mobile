import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:senme/core/config_core.dart';
import 'package:senme/dto/category.dart';
import 'package:senme/gifts/gift_page.dart';
import 'package:senme/mainscreen/main_screen_bloc.dart';
import 'package:senme/mainscreen/main_screen_state.dart';
import 'package:senme/profile/profile_widget.dart';
import 'package:senme/ui/buttons/general_button.dart';
import 'package:senme/ui/widgets/circle_indicator.dart';
import 'package:senme/ui/widgets/custom_bottom_navigation_bar.dart';
import 'package:senme/core/utils.dart';

import 'main_screen_event.dart';

class MainScreenPage extends StatelessWidget {
  MainScreenPage({
    Key key,
    this.title,
  }) : super(key: key);

  final String title;

  final List<BottomNavigationBarItem> items = [
    BottomNavigationBarItem(
        // icon: SvgPicture.asset("images/card.svg"),
        // activeIcon: SvgPicture.asset("images/card.svg",color: ConfigCore.colorPrimaryColor),
        icon: const ImageIcon(
          AssetImage("images/heart.png"),
          size: 32,
        ),
        label: 'Senme'),
    BottomNavigationBarItem(
        // icon: SvgPicture.asset("images/calendar.svg"),
        icon: const ImageIcon(
          AssetImage("images/calendar.png"),
          size: 32,
        ),
        label: 'События'),
    BottomNavigationBarItem(
        icon: const ImageIcon(
          AssetImage("images/orders.png"),
          size: 32,
        ),
        label: 'Заказы'),
    BottomNavigationBarItem(
        icon: const Icon(
          Icons.account_circle,
          size: 32,
        ),
        label: 'Профиль'),
  ];

  final List<Widget> pages = [
    CategoriesWidget(),
    OrdersWidget(),
    ProfileWidget()
  ];

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) {
        Category scroll = Category(
            name: "Свиток c посланием!",
            description:
                "Цветовая гамма свитка и запах подбирается индивидуально!",
            imageName: "scroll_cards.png",
            buttonText: "Оформить",
            buttonBackgroundColor: ConfigCore.colorPrimaryColor,
            buttonTextColor: Colors.white);

        Category storyCard = Category(
            name: "Открытка с сюжетом!",
            description:
                "Оформите поздравительную открытку, добавив личное послание и многое другое",
            imageName: "story_cards.png",
            buttonText: "Перейти к оформлению",
            buttonBackgroundColor: Color(0xff458500),
            buttonTextColor: Colors.white);

        Category cardWithPhoto = Category(
            name: "Открытка с фото!",
            description:
                "Ваши фотографии, оформленные в виде уникальной почтовой открытки",
            imageName: "photo_cards.png",
            buttonText: "Перейти к оформлению",
            buttonBackgroundColor: Color(0xff458500),
            buttonTextColor: Colors.white);

        List<Category> categories = [
          scroll,
          storyCard,
          cardWithPhoto,
        ];
        return MainScreenBloc(
            MainScreenStateInitial(0, ConfigCore.appName, false), categories);
      },
      child: BlocBuilder<MainScreenBloc, MainScreenState>(
        builder: (context, state) {
          return Scaffold(
            appBar: AppBar(
              centerTitle: true,
              backgroundColor: Colors.transparent,
              elevation: 0.0,
              toolbarHeight: !state.isShowAppBar ? 0 : kToolbarHeight,
              title: Text(
                '',
                style: TextStyle(
                    color: Color(0xff95A5BC),
                    fontWeight: FontWeight.w700,
                    fontSize: 30),
              ),
            ),
            body: pages[state.selectedPage],
            bottomNavigationBar: CustomBottomNavigationBar(
              onTap: (int index) {
                BlocProvider.of<MainScreenBloc>(context).add(PageChanged(
                    selectedPage: index,
                    title: items[index].label,
                    isShowAppBar: index != 0));
              },
            ),
          );
        },
      ),
    );
  }
}

class CategoriesWidget extends StatefulWidget {
  const CategoriesWidget({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => CategoriesStateWidget();
}

class CategoriesStateWidget extends State<CategoriesWidget> {
  int currentPageValue = 0;
  Widget getCircleIndicator(bool isActive) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      margin: EdgeInsets.symmetric(horizontal: 8),
      height: isActive ? 12 : 8,
      width: isActive ? 12 : 8,
      child: Image.asset(
        'images/${isActive ? 'active_dote.png' : 'inactive_dote.png'}',
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    MainScreenBloc mainScreenBloc = BlocProvider.of<MainScreenBloc>(context);
    final PageController _pageController =
        PageController(initialPage: currentPageValue);
    currentPageValue = _pageController.initialPage;

    String buttonText = 'Оформить';
    final TextStyle textStyle = TextStyle(
        fontSize: 24,
        fontWeight: FontWeight.w500);
    final size = Utils.getTextSize(buttonText,textStyle);
    final Shader linearGradient = LinearGradient(
      colors: <Color>[Color(0xffEA152F), Color(0xff7215EA)],
    ).createShader(Rect.fromLTWH(113.5, 24, size.width, size.height));

    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        PageView.builder(
          itemBuilder: (context, index) {
            var item = mainScreenBloc.categories[index];
            return CategoryItemWidget(item: item);
          },
          itemCount: mainScreenBloc.categories.length,
          controller: _pageController,
          scrollDirection: Axis.horizontal,
          onPageChanged: (page) {
            setState(() {
              currentPageValue = page;
            });
          },
        ),
        Stack(
          alignment: AlignmentDirectional.bottomCenter,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: 35),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  MaterialButton(
                      onPressed: () => {},
                      minWidth: MediaQuery.of(context).size.width-30,
                      height: 80,
                      child: Text(
                        buttonText,
                        style: TextStyle(
                            foreground: Paint()..shader = linearGradient,
                            fontSize: 24,
                            fontWeight: FontWeight.w500),
                      ),
                      color: Color(0xffF4FBFF),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20))),
                  SizedBox(height: 30,),
                  SizedBox(height: 15,child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      for (int i = 0; i < mainScreenBloc.categories.length; i++)
                        getCircleIndicator(i == currentPageValue)
                    ],
                  ),)
                ],
              ),
            ),
          ],
        )
      ],
    );
  }
}

class CategoryItemWidget extends StatelessWidget {
  const CategoryItemWidget({
    Key key,
    @required this.item,
  }) : super(key: key);

  final Category item;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        alignment: Alignment.topCenter,
        children: [
          Padding(
            padding: EdgeInsets.only(top: 16),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  item.name,
                  style: TextStyle(
                      color: Color(0xff95A5BC),
                      fontWeight: FontWeight.w700,
                      fontSize: 30),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 25, right: 25),
                  child: Text(
                    item.description,
                    style: TextStyle(
                        color: Color(0xff95A5BC), fontWeight: FontWeight.w400),
                  ),
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Image.asset('images/${item.imageName}'),
          ),
        ],
      ),
    );
  }
}

class OrdersWidget extends StatelessWidget {
  const OrdersWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MainScreenBloc mainScreenBloc = BlocProvider.of<MainScreenBloc>(context);
    return Stack(
      children: [Text('OrdersWidget')],
    );
  }
}
