import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class MainScreenEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class PageChanged extends MainScreenEvent {
  int selectedPage = 0;
  String title;
  bool isShowAppBar;
  PageChanged(
      {@required this.selectedPage,
      @required this.title,
      @required this.isShowAppBar});
}
