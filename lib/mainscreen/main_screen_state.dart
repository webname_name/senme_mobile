import 'package:equatable/equatable.dart';
import 'package:senme/core/config_core.dart';

class MainScreenState extends Equatable {
  int selectedPage = 0;
  String title;
  bool isShowAppBar = true;

  MainScreenState(
      {this.selectedPage,
      this.title = ConfigCore.appName,
      this.isShowAppBar = true});

  @override
  List<Object> get props => [selectedPage, title, isShowAppBar];
}

class MainScreenStateInitial extends MainScreenState {
  MainScreenStateInitial(int selectedPage, String title, bool isShowAppBar)
      : super(
            selectedPage: selectedPage,
            title: title,
            isShowAppBar: isShowAppBar);
}

class MainScreenStatePageChanged extends MainScreenState {
  MainScreenStatePageChanged(int selectedPage, String title, bool isShowAppBar)
      : super(
            selectedPage: selectedPage,
            title: title,
            isShowAppBar: isShowAppBar);
}
