import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:senme/core/config_core.dart';
import 'package:senme/dto/category.dart';
import 'package:senme/gifts/gift_page.dart';
import 'package:senme/mainscreen/main_screen_bloc.dart';
import 'package:senme/mainscreen/main_screen_state.dart';
import 'package:senme/profile/profile_widget.dart';

import 'main_screen_event.dart';

class MainScreenPageOld extends StatelessWidget {
  MainScreenPageOld({
    Key key,
    this.title,
  }) : super(key: key);

  final String title;

  final List<BottomNavigationBarItem> items = [
    BottomNavigationBarItem(
        // icon: SvgPicture.asset("images/card.svg"),
        // activeIcon: SvgPicture.asset("images/card.svg",color: ConfigCore.colorPrimaryColor),
        icon: const ImageIcon(
          AssetImage("images/heart.png"),
          size: 32,
        ),
        label: 'Senme'),
    BottomNavigationBarItem(
        // icon: SvgPicture.asset("images/calendar.svg"),
        icon: const ImageIcon(
          AssetImage("images/calendar.png"),
          size: 32,
        ),
        label: 'События'),
    BottomNavigationBarItem(
        icon: const ImageIcon(
          AssetImage("images/orders.png"),
          size: 32,
        ),
        label: 'Заказы'),
    BottomNavigationBarItem(
        icon: const Icon(
          Icons.account_circle,
          size: 32,
        ),
        label: 'Профиль'),
  ];

  final List<Widget> pages = [
    CategoriesWidget(),
    EventsWidget(),
    OrdersWidget(),
    ProfileWidget()
  ];

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) {
        Category scroll = Category(
            name: "Свиток\n(с сургучной печатью)",
            description:
                "Цветовая гамма свитка и запах подбирается индивидуально!",
            imageName: "scroll_category.png",
            buttonText: "Перейти к оформлению",
            buttonBackgroundColor: ConfigCore.colorPrimaryColor,
            buttonTextColor: Colors.white);
        Category cardWithPhoto = Category(
            name: "Открытка\n(с фотографией)",
            description:
                "Ваши фотографии, оформленные в виде уникальной почтовой открытки",
            imageName: "card_with_photo.png",
            buttonText: "Перейти к оформлению",
            buttonBackgroundColor: Color(0xff458500),
            buttonTextColor: Colors.white);
        Category congratulatoryCard = Category(
            name: "Открытка\n(поздравительная)",
            description:
                "Оформите поздравительную открытку, добавив личное послание и многое другое",
            imageName: "congratulatory_сard.png",
            buttonText: "Перейти к оформлению",
            buttonBackgroundColor: Color(0xff458500),
            buttonTextColor: Colors.white);
        Category manGift = Category(
            name: "Подарок мужчине",
            description:
                "Готовый подарок мужчине: клатч на кнопке и ремень из кожи в оригинальной упаковке",
            imageName: "man_gift.png",
            genderId: 1,
            buttonText: "Заказать",
            buttonBackgroundColor: Colors.white,
            buttonTextColor: Color(0xff458500));
        Category womenGift = Category(
            name: "Подарок девушке",
            description:
                "Готовый подарок мужчине: клатч на кнопке и ремень из кожи в оригинальной упаковке",
            imageName: "women_gift.png",
            genderId: 0,
            buttonText: "Заказать",
            buttonBackgroundColor: Colors.white,
            buttonTextColor: Color(0xff458500));

        Category cardsOrInvitationsSet = Category(
            name: "Набор открыток или приглашения",
            description:
                "Отправляйте настоящие почтовые открытки с личными пожеланиями в любую точку мира",
            imageName: "cards_or_invitations_set.png",
            buttonText: "Перейти к оформлению",
            buttonBackgroundColor: Color(0xff458500),
            buttonTextColor: Colors.white);

        Category predictions = Category(
            name: "Предсказания Омикудзе",
            description:
                "Потряси телефон и отправь предсказание себе или своим любимым",
            imageName: "predictions.png",
            buttonText: "Перейти к оформлению",
            buttonBackgroundColor: Color(0xff458500),
            buttonTextColor: Colors.white);

        List<Category> categories = [
          scroll,
          cardWithPhoto,
          congratulatoryCard,
          // manGift,
          // womenGift,
          cardsOrInvitationsSet,
          predictions
        ];
        return MainScreenBloc(
            MainScreenStateInitial(0, ConfigCore.appName, true), categories);
      },
      child: BlocBuilder<MainScreenBloc, MainScreenState>(
        builder: (context, state) {
          MainScreenBloc mainScreenBloc =
              BlocProvider.of<MainScreenBloc>(context);
          return Scaffold(
              appBar: AppBar(
                title: Text(state.title),
                centerTitle: true,
                toolbarHeight: !state.isShowAppBar ? 0 : kToolbarHeight,
                /*actions: [
                IconButton(
                    icon: SvgPicture.asset(
                      "images/ring.svg",
                      color: Colors.white,
                    ),
                    onPressed: () => {})
              ],*/
              ),
              body: pages[state.selectedPage],
              bottomNavigationBar: BottomNavigationBar(
                items: items,
                currentIndex: state.selectedPage,
                showUnselectedLabels: false,
                backgroundColor: Color(0xffFCFCFE),
                selectedItemColor: Colors.black,
                unselectedItemColor: Color(0xffD0C9D6),
                selectedLabelStyle:
                    TextStyle(color: Color(0xff333333), fontSize: 12),
                type: BottomNavigationBarType.fixed,
                onTap: (int index) {
                  BlocProvider.of<MainScreenBloc>(context).add(PageChanged(
                      selectedPage: index,
                      title: items[index].label,
                      isShowAppBar: true));
                },
              ));
        },
      ),
    );
  }
}

class CategoriesWidget extends StatelessWidget {
  const CategoriesWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MainScreenBloc mainScreenBloc = BlocProvider.of<MainScreenBloc>(context);
    return Stack(
      children: [
        ListView.builder(
            itemBuilder: (context, index) {
              var item = mainScreenBloc.categories[index];
              return InkWell(
                onTap: () {
                  // Scaffold.of(context).showSnackBar(SnackBar(
                  //   content: Text(item.name),duration: Duration(seconds: 1),
                  // ));
                  switch (index) {
                    case 0:
                      {
                        break;
                      }
                    case 1:
                      {
                        break;
                      }
                    case 2:
                      {
                        break;
                      }
                    case 3:
                      {
                        // Navigator.push(context, MaterialPageRoute(builder: (context) => GiftPage(title:item.name,categoryJson: item.toJson().toString(),genderId: item.genderId)));
                        break;
                      }
                    case 4:
                      {
                        // Navigator.push(context, MaterialPageRoute(builder: (context) => GiftPage(title:item.name, genderId: item.genderId)));
                        break;
                      }
                    case 5:
                      {
                        break;
                      }
                    case 6:
                      {
                        break;
                      }
                  }
                },
                child: CategoryItemWidget(item: item),
              );
            },
            itemCount: mainScreenBloc.categories.length)
      ],
    );
  }

  Route<Object> createGiftPageRoute(MainScreenBloc mainScreenBloc) {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) =>
          BlocProvider<MainScreenBloc>(
        create: (context) => mainScreenBloc,
        child: GiftPage(),
      ),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(0.0, 1.0);
        var end = Offset.zero;
        var curve = Curves.ease;

        var tween =
            Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }
}

/*class CategoryItemWidgetOld extends StatelessWidget {
  const CategoryItemWidgetOld({
    Key key,
    @required this.item,
  }) : super(key: key);

  final Category item;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 120,
      color: Colors.white,
      child: Card(
        elevation: 6,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            SizedBox(
              width: 120,
              child: Image.asset("images/${item.imageName}"),
            ),
            Expanded(
              child: Container(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(15, 10, 5, 10),
                  child: Column(
                    crossAxisAlignment:
                    CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(item.name,
                          style: TextStyle(
                              color: Color(0xff333333),
                              fontSize: 16)),
                      SizedBox(height: 10,),
                      Text(
                        item.description,
                        style: TextStyle(
                            color: Color(0xff828282),
                            fontSize: 12),
                      ),
                      // FlatButton(textColor: item.buttonTextColor,color: item.buttonBackgroundColor,child: Text(item.buttonText),)
                      Expanded(
                        child: Align(
                          child: RaisedButton(
                              onPressed: () {},
                              child: Text(
                                item.buttonText,
                                style: TextStyle(
                                    color:
                                    item.buttonTextColor),
                              ),
                              color:
                              item.buttonBackgroundColor),
                          alignment:
                          Alignment.bottomLeft,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}*/

class CategoryItemWidget extends StatelessWidget {
  const CategoryItemWidget({
    Key key,
    @required this.item,
  }) : super(key: key);

  final Category item;

  @override
  Widget build(BuildContext context) {
    final double screenWidth = MediaQuery.of(context).size.width;
    final double paddingLeft = screenWidth * 0.02;
    final double imageContainerMargin = 8.0;
    final double imageContainerWidth = 164.0 + imageContainerMargin * 2;
    final buttonWidth = (screenWidth - paddingLeft * 2) - imageContainerWidth;

    return Container(
      margin: EdgeInsets.only(top: 9, left: paddingLeft, right: paddingLeft),
      height: 200,
      child: Card(
        elevation: 4,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.all(imageContainerMargin),
              child: Image.asset(
                "images/${item.imageName}",
                width: 164,
                height: 164,
                fit: BoxFit.cover,
              ),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                height: double.infinity,
                margin: EdgeInsets.only(top: 8),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Padding(
                        padding: EdgeInsets.only(
                          right: 5,
                        ),
                        child: Text(item.name,
                            style: TextStyle(
                                color: Color(0xff333333),
                                fontSize: 16,
                                fontWeight: FontWeight.w700)),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10, bottom: 10),
                      alignment: Alignment.topLeft,
                      child: Container(
                        color: ConfigCore.colorPrimary,
                        width: 53.5,
                        height: 2,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 5),
                      child: Text(
                        item.description,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 12,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                    Container(
                      child: MaterialButton(
                        minWidth: buttonWidth,
                        height: 50,
                        onPressed: () {},
                        child: Text(
                          'Оформить',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.w500),
                        ),
                        color: ConfigCore.colorPrimary,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10),
                                bottomRight: Radius.circular(10))),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class EventsWidget extends StatelessWidget {
  const EventsWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MainScreenBloc mainScreenBloc = BlocProvider.of<MainScreenBloc>(context);
    return Stack(
      children: [Text('Events')],
    );
  }
}

class OrdersWidget extends StatelessWidget {
  const OrdersWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MainScreenBloc mainScreenBloc = BlocProvider.of<MainScreenBloc>(context);
    return Stack(
      children: [Text('OrdersWidget')],
    );
  }
}
