// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dbfile.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DbFile _$DbFileFromJson(Map<String, dynamic> json) {
  return DbFile(
    id: json['id'] as int,
    uuid: json['uuid'] as String,
    type: _$enumDecodeNullable(_$EFileTypeEnumMap, json['type']),
  );
}

Map<String, dynamic> _$DbFileToJson(DbFile instance) => <String, dynamic>{
      'id': instance.id,
      'uuid': instance.uuid,
      'type': _$EFileTypeEnumMap[instance.type],
    };

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

T _$enumDecodeNullable<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source, unknownValue: unknownValue);
}

const _$EFileTypeEnumMap = {
  EFileType.IMAGE: 'IMAGE',
  EFileType.VIDEO: 'VIDEO',
  EFileType.PDF: 'PDF',
  EFileType.WORD: 'WORD',
};
