import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

part 'category.g.dart';

@JsonSerializable()
class Category {
  final id;
  final String name;
  final String description;
  final String imageName;
  final int genderId;
  final String buttonText;

  @JsonKey(ignore: true)
  final Color buttonBackgroundColor;

  @JsonKey(ignore: true)
  final Color buttonTextColor;

  Category(
      {this.id,
      this.name,
      this.description,
      this.imageName,
      this.genderId,
      this.buttonText,
      this.buttonBackgroundColor,
      this.buttonTextColor});
  factory Category.fromJson(Map<String, dynamic> json) =>
      _$CategoryFromJson(json);
  Map<String, dynamic> toJson() => _$CategoryToJson(this);
}
