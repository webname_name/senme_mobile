import 'package:json_annotation/json_annotation.dart';

part 'user_registration_form.g.dart';

@JsonSerializable()
class UserRegistrationForm {
  final String username;
  final String phoneNumber;
  final String email;
  final String password;

  UserRegistrationForm(
      this.username, this.phoneNumber, this.email, this.password);

  factory UserRegistrationForm.fromJson(Map<String, dynamic> json) =>
      _$UserRegistrationFormFromJson(json);

  Map<String, dynamic> toJson() => _$UserRegistrationFormToJson(this);
}
