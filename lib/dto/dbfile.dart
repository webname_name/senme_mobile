import 'package:json_annotation/json_annotation.dart';
import 'package:senme/enums/etype_file.dart';

part 'dbfile.g.dart';

@JsonSerializable()
class DbFile {
  int id;
  String uuid;
  EFileType type;
  DbFile({this.id, this.uuid, this.type});

  factory DbFile.fromJson(Map<String, dynamic> json) => _$DbFileFromJson(json);
  Map<String, dynamic> toJson() => _$DbFileToJson(this);
}
