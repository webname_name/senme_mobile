// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_registration_form.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserRegistrationForm _$UserRegistrationFormFromJson(Map<String, dynamic> json) {
  return UserRegistrationForm(
    json['username'] as String,
    json['phoneNumber'] as String,
    json['email'] as String,
    json['password'] as String,
  );
}

Map<String, dynamic> _$UserRegistrationFormToJson(
        UserRegistrationForm instance) =>
    <String, dynamic>{
      'username': instance.username,
      'phoneNumber': instance.phoneNumber,
      'email': instance.email,
      'password': instance.password,
    };
