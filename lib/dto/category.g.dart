// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'category.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Category _$CategoryFromJson(Map<String, dynamic> json) {
  return Category(
    id: json['id'],
    name: json['name'] as String,
    description: json['description'] as String,
    imageName: json['imageName'] as String,
    genderId: json['genderId'] as int,
    buttonText: json['buttonText'] as String,
  );
}

Map<String, dynamic> _$CategoryToJson(Category instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
      'imageName': instance.imageName,
      'genderId': instance.genderId,
      'buttonText': instance.buttonText,
    };
