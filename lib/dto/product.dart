import 'package:json_annotation/json_annotation.dart';
import 'package:senme/dto/dbfile.dart';
import 'package:senme/enums/etype_file.dart';

part 'product.g.dart';

@JsonSerializable()
class Product {
  int id;
  String name;
  // ProductType type;
  double price;
  int points;
  int discountPercentage;
  DateTime discountExpiredDate;
  String description;
  String shortDescription;
  int genderId; //0 = female 1 = male, NULL unknown
  bool isTop;
  List<DbFile> dbFiles = [];

  Product(
      this.id,
      this.name,
      this.price,
      this.points,
      this.discountPercentage,
      this.discountExpiredDate,
      this.description,
      this.shortDescription,
      this.genderId,
      this.isTop);

  factory Product.fromJson(Map<String, dynamic> json) =>
      _$ProductFromJson(json);

  Map<String, dynamic> toJson() => _$ProductToJson(this);

  String get photoUIID {
    List<DbFile> photos =
        dbFiles.where((element) => element.type == EFileType.IMAGE).toList();
    return photos.length == 0 ? null : photos.first?.uuid;
  }

  double get priceWithDiscount => price - (price * discountPercentage / 100);
}
