// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Product _$ProductFromJson(Map<String, dynamic> json) {
  return Product(
    json['id'] as int,
    json['name'] as String,
    (json['price'] as num)?.toDouble(),
    json['points'] as int,
    json['discountPercentage'] as int,
    json['discountExpiredDate'] == null
        ? null
        : DateTime.parse(json['discountExpiredDate'] as String),
    json['description'] as String,
    json['shortDescription'] as String,
    json['genderId'] as int,
    json['isTop'] as bool,
  )..dbFiles = (json['dbFiles'] as List)
      ?.map(
          (e) => e == null ? null : DbFile.fromJson(e as Map<String, dynamic>))
      ?.toList();
}

Map<String, dynamic> _$ProductToJson(Product instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'price': instance.price,
      'points': instance.points,
      'discountPercentage': instance.discountPercentage,
      'discountExpiredDate': instance.discountExpiredDate?.toIso8601String(),
      'description': instance.description,
      'shortDescription': instance.shortDescription,
      'genderId': instance.genderId,
      'isTop': instance.isTop,
      'dbFiles': instance.dbFiles,
    };
