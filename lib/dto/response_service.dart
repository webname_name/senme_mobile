import 'package:equatable/equatable.dart';

class ResponseService extends Equatable {
  final dynamic data;
  final int statusCode;
  ResponseService(this.data, this.statusCode);

  @override
  List<Object> get props => [data, statusCode];
}
