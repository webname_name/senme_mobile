import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:senme/api/api_service.dart';
import 'package:senme/core/config_core.dart';
import 'package:senme/dto/product.dart';
import 'package:senme/gifts/gift_bloc.dart';
import 'package:senme/gifts/gift_event.dart';
import 'package:senme/gifts/gift_state.dart';
import 'package:senme/timer/timer_widget.dart';

class GiftPage extends StatelessWidget {
  const GiftPage({Key key, this.title, this.categoryJson, this.genderId})
      : super(key: key);
  final String title;
  final String categoryJson;
  final int genderId;

  @override
  Widget build(BuildContext context) {
    print('json: $categoryJson');
    double screenWidth = MediaQuery.of(context).size.width;
    print('screenWidth $screenWidth');
    return BlocProvider<GiftBloc>(
      create: (context) {
        return GiftBloc(GiftState()..genderId = genderId)
          ..add(GiftEventInitial());
      },
      child: BlocListener<GiftBloc, GiftState>(
        listener: (context, state) {
          print('data: ${state.data.length}');
        },
        child: BlocBuilder<GiftBloc, GiftState>(
          builder: (context, state) {
            return Scaffold(
              appBar: AppBar(
                title: Text(title),
              ),
              /*body: Stack(
                children: [
                  Padding(
                    padding: EdgeInsets.all(8),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                            padding: EdgeInsets.all(15),
                            child: Text('Получайте дополнительные бонусы при добавлении подарков к заказу!',
                              style: TextStyle(color: Color(0xff4F4F4F), fontSize: 14),textAlign: TextAlign.center,)),
                        SizedBox(height: 10,),
                        Expanded(
                          child: GridView.builder(
                            itemBuilder: (context, index){
                              var item = state.data[index];
                              final double screenWidth = MediaQuery.of(context).size.width;
                              print('screenWidth: $screenWidth');
                              return Stack(
                                children: [
                                  Card(
                                    elevation: 6,
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment:  CrossAxisAlignment.start,
                                      children: [
                                        // Image.network('${ApiService.BASE_URL}/open/api/file/download/${item.photoUIID}',),
                                        ClipRRect(
                                          child: CachedNetworkImage(
                                            imageUrl: "${ApiService.BASE_URL}/open/api/file/download/${item.photoUIID}",
                                            imageBuilder: (context, imageProvider) => Container(
                                              height: 150,
                                              decoration: BoxDecoration(
                                                image: DecorationImage(
                                                    image: imageProvider,
                                                    fit: BoxFit.fill),
                                              ),
                                            ),
                                            // placeholder: (context, url) => CircularProgressIndicator(),
                                          ),
                                          borderRadius: BorderRadius.only(topLeft: Radius.circular(6),topRight: Radius.circular(6)),
                                        ),
                                        Container(
                                          padding: EdgeInsets.all(10),
                                          child: Column(
                                            mainAxisSize: MainAxisSize.max,
                                            children: [
                                              Text(item.name,style: TextStyle(fontSize: 16, color: Color(0xff333333)),textAlign: TextAlign.start,),
                                              Text(item.shortDescription,style: TextStyle(fontSize: 12, color: Color(0xff828282)),textAlign: TextAlign.start,),
                                              if(item.discountPercentage>0 && item.discountExpiredDate.isBefore(DateTime.now()))
                                                Row(
                                                  mainAxisSize: MainAxisSize.max,
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Text('Цена:',style: TextStyle(fontSize: 12,color: Color(0xff828282)),),
                                                    Text('${item.price.toStringAsFixed(0)}',style: TextStyle(fontSize: 14,color: Color(0xff828282),decoration: TextDecoration.lineThrough)),
                                                    Text('${item.priceWithDiscount.toStringAsFixed(0)}T',style: TextStyle(fontSize: 16,color: ConfigCore.colorPrimaryColor,)),
                                                  ],
                                                )else Row(
                                                mainAxisSize: MainAxisSize.max,
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  Text('Цена:',style: TextStyle(fontSize: 12,color: Color(0xff828282)),),
                                                  Text('${item.price.toStringAsFixed(0)}T',style: TextStyle(fontSize: 16,color: Colors.black,)),
                                                ],
                                              ),
                                              Align(child: MaterialButton(
                                                minWidth: double.infinity,
                                                padding: EdgeInsets.only(left: 8),
                                                onPressed: (){},
                                                color: ConfigCore.colorPrimaryColor,child: Text('Добавить',style: TextStyle(color: Colors.white, fontSize: 12),),),
                                                alignment: Alignment.bottomCenter,
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Stack(
                                    children: [
                                      Align(child: Container(child: SvgPicture.asset("images/top.svg",),width: 24,height: 24,),alignment: Alignment.topLeft,),
                                      Align(child: Container(child: Text('Топ',style: TextStyle(color: Colors.white, fontSize: 8)),padding: EdgeInsets.only(top: 6,left: 5),)
                                        ,alignment: Alignment.topLeft,)
                                    ],
                                  )
                                ] ,
                              );
                            },
                            itemCount: state.data.length,
                            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2,
                                mainAxisSpacing: 10,
                                crossAxisSpacing: 5,
                                childAspectRatio: 0.55
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    alignment: Alignment.center,
                    child: state.isShowProgress
                        ? CircularProgressIndicator(
                      backgroundColor: Colors.white12,
                      valueColor: AlwaysStoppedAnimation<Color>(ConfigCore.colorPrimaryColor),
                    )
                        : null,
                  )
                ],
              ),*/
              body: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  child: Stack(
                    children: [
                      Padding(
                          padding: EdgeInsets.all(15),
                          child: Text(
                            'Получайте дополнительные бонусы при добавлении подарков к заказу!',
                            style: TextStyle(
                                color: Color(0xff4F4F4F), fontSize: 14),
                            textAlign: TextAlign.center,
                          )),
                      SizedBox(
                        height: 10,
                      ),
                      ListView.builder(
                        primary: false,
                        shrinkWrap: true,
                        itemBuilder: (context, index) =>
                            ProductItemWidget(item: state.data[index]),
                        itemCount: state.data.length,
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: Container(
                          alignment: Alignment.center,
                          child: state.isShowProgress
                              ? CircularProgressIndicator(
                                  backgroundColor: Colors.white12,
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      ConfigCore.colorPrimaryColor),
                                )
                              : null,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}

class ProductItemWidget extends StatelessWidget {
  ProductItemWidget({
    Key key,
    @required this.item,
  }) : super(key: key);
  final Product item;
  final double generalMargin = 15;
  final double columnPadding = 10;

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double buttonWidth =
        (screenWidth - (generalMargin * 2) - (columnPadding * 3)) / 2;
    return Stack(
      children: [
        Card(
          margin: EdgeInsets.all(generalMargin),
          elevation: 6,
          child: Container(
            height: 420,
            child: Stack(
              children: [
                Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ClipRRect(
                      child: CachedNetworkImage(
                          fit: BoxFit.cover,
                          imageUrl:
                              "${ApiService.BASE_URL}/open/api/file/download/${item.photoUIID}",
                          imageBuilder: (context, imageProvider) => Container(
                                height: 200,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: imageProvider, fit: BoxFit.fill),
                                ),
                              )
                          // placeholder: (context, url) => CircularProgressIndicator(),
                          ),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(6),
                          topRight: Radius.circular(6)),
                    ),
                    Container(
                      padding: EdgeInsets.all(columnPadding),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Text(
                            item.name,
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Color(0xff333333)),
                            textAlign: TextAlign.start,
                          ),
                          Text(
                            item.shortDescription,
                            style: TextStyle(
                                fontSize: 16, color: Color(0xff828282)),
                            textAlign: TextAlign.start,
                          ),
                          SizedBox(
                            height: 25,
                          ),
                          if (item.discountPercentage > 0 &&
                              (item.discountExpiredDate == null ||
                                  item.discountExpiredDate
                                      .isAfter(DateTime.now())))
                            Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Цена:',
                                  style: TextStyle(
                                      fontSize: 16, color: Color(0xff828282)),
                                ),
                                Text('${item.price.toStringAsFixed(0)}',
                                    style: TextStyle(
                                        fontSize: 14,
                                        color: Color(0xff828282),
                                        decoration:
                                            TextDecoration.lineThrough)),
                                Text(
                                    '${item.priceWithDiscount.toStringAsFixed(0)}T',
                                    style: TextStyle(
                                      fontSize: 16,
                                      color: ConfigCore.colorPrimaryColor,
                                    )),
                              ],
                            )
                          else
                            Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Цена:',
                                  style: TextStyle(
                                      fontSize: 16, color: Color(0xff828282)),
                                ),
                                Text('${item.price.toStringAsFixed(0)}T',
                                    style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.black,
                                    )),
                              ],
                            ),
                          SizedBox(
                            height: 5,
                          ),
                          if (item.discountExpiredDate != null &&
                              item.discountExpiredDate.isAfter(DateTime.now()))
                            TimerWidget(
                              expiredDate: item.discountExpiredDate,
                              callback: () => BlocProvider.of<GiftBloc>(context)
                                  .add(GiftEventDataChanged()),
                            ),
                          SizedBox(
                            height: 5,
                          ),
                          Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              MaterialButton(
                                minWidth: buttonWidth,
                                padding: EdgeInsets.only(left: 5, right: 5),
                                onPressed: () {},
                                shape: Border.all(color: ConfigCore.greyColor),
                                color: Colors.white,
                                child: Row(
                                  children: [
                                    Icon(
                                      Icons.shopping_cart,
                                      color: ConfigCore.greyColor,
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Text('В корзину',
                                        style: TextStyle(
                                            color: ConfigCore.greyColor,
                                            fontSize: 12)),
                                  ],
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                ),
                              ),
                              SizedBox(
                                width: columnPadding,
                              ),
                              MaterialButton(
                                  minWidth: buttonWidth,
                                  padding: EdgeInsets.only(left: 5, right: 5),
                                  onPressed: () {},
                                  color: ConfigCore.colorPrimaryColor,
                                  child: Text(
                                    'Купить',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 12),
                                  )),
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.all(7.5),
          child: Container(
            alignment: Alignment.topLeft,
            width: 36,
            height: 36,
            margin: EdgeInsets.only(right: 10, bottom: 10),
            child: Stack(
              children: [
                Align(
                  child: Container(
                    child: SvgPicture.asset(
                      "images/top.svg",
                    ),
                    width: 36,
                    height: 36,
                  ),
                  alignment: Alignment.center,
                ),
                Align(
                  child: Container(
                    child: Text('Топ',
                        style: TextStyle(color: Colors.white, fontSize: 12)),
                  ),
                  alignment: Alignment.center,
                )
              ],
            ),
          ),
        ),
        Positioned(
          top: 200,
          right: generalMargin,
          child: Container(
            alignment: Alignment.center,
            decoration:
                BoxDecoration(color: Color(0xffEB5757), shape: BoxShape.circle),
            width: 36,
            height: 36,
            child: Text(
              '${item.discountPercentage} %',
              style: TextStyle(color: Colors.white, fontSize: 12),
            ),
          ),
        )
      ],
    );
  }
}
