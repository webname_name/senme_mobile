import 'package:equatable/equatable.dart';
import 'package:senme/dto/product.dart';

class GiftState extends Equatable {
  List<Product> data = [];
  int genderId = null;
  bool isShowProgress = false;

  @override
  List<Object> get props => [data, genderId, isShowProgress];
}
