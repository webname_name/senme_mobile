import 'package:equatable/equatable.dart';

class GiftEvent extends Equatable {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class GiftEventInitial extends GiftEvent {}

class GiftEventDataChanged extends GiftEvent {}
