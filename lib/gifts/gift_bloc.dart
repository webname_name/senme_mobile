import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:senme/api/api_service.dart';
import 'package:senme/dto/product.dart';
import 'package:senme/gifts/gift_event.dart';
import 'package:senme/gifts/gift_state.dart';

class GiftBloc extends Bloc<GiftEvent, GiftState> {
  GiftBloc(GiftState initialState) : super(initialState);
  var _apiService = new ApiService();

  @override
  Stream<GiftState> mapEventToState(GiftEvent event) async* {
    if (event is GiftEventInitial) {
      yield GiftState()
        ..genderId = state.genderId
        ..isShowProgress = true;
      List<Product> data = await _apiService.getGifts(state.genderId);
      yield GiftState()..data = data;
    }
    if (event is GiftEventDataChanged) {
      GiftState newState = GiftState();
      newState.data = state.data;
      newState.genderId = state.genderId;
      newState.isShowProgress = true;
      yield newState;
      yield await Future.delayed(
          Duration(seconds: 1), () => state..isShowProgress = false);
    }
  }
}
