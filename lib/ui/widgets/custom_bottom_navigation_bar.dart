import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CustomBottomNavigationBar extends StatefulWidget {
  final ValueChanged<int> onTap;

  const CustomBottomNavigationBar({Key key, this.onTap}) : super(key: key);
  @override
  State<StatefulWidget> createState() => _CustomBottomNavigationBarState();
}

class _CustomBottomNavigationBarState extends State<CustomBottomNavigationBar> {
  int selectedPage=0;
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 80,
        margin: EdgeInsets.only(bottom: Platform.isIOS ? 35 : 16),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 45.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              InkWell(
                onTap: ()=> _changePage(widget.onTap, 0),
                child: BottomNavigationItem(
                    assetName: 'images/heart.svg', inActiveAssetName: 'images/in_active_heart.svg',isActive: selectedPage==0),
              ),
              InkWell(
                onTap: ()=>_changePage(widget.onTap, 1),
                child: BottomNavigationItem(
                    assetName: 'images/package.svg', inActiveAssetName: 'images/in_active_package.svg',  isActive: selectedPage==1),
              ),
              InkWell(
                onTap: ()=>_changePage(widget.onTap, 2),
                child: BottomNavigationItem(
                    assetName: 'images/profile.svg', inActiveAssetName:'images/in_active_profile.svg',isActive: selectedPage==2),
              )
            ],
          ),
        ));
  }

  _changePage(ValueChanged<int> onTap, int index) {
    onTap(index);
    setState(() {
      selectedPage = index;
    });
  }
}



class BottomNavigationItem extends StatelessWidget {
  final String assetName;
  final inActiveAssetName;
  final bool isActive;



  const BottomNavigationItem({
    Key key,
    @required this.assetName,
     @required this.inActiveAssetName,
    this.isActive,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    /*return Container(
      width: 84,
      height: 84,
      decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage(
              'images/${isActive ? 'active_navigation_button.png' : 'inactive_navigation_button.png'}',
            ),
            fit: BoxFit.cover),
      ),
      child: Center(
        child: SvgPicture.asset(
          assetName,
          color: Color(0xff95A5BC),
          width: 32,
          height: 28,
        ),
      ),
    );*/
    return Stack(
      alignment: Alignment.center,
      children: [
        Image.asset('images/${isActive ? 'active_navigation_button.png' : 'inactive_navigation_button.png'}'),
        SvgPicture.asset(
          isActive ? assetName : inActiveAssetName,
          color: Color(0xff95A5BC),
          width: 32,
          height: 28,
        )
      ],
    );
  }
}
