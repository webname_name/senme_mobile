import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AboutAppScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('О приложении'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          margin: EdgeInsets.only(top: 114, left: 21, right: 21),
          child: Column(
            children: [
              SvgPicture.asset(
                "images/heart.svg",
                height: 58,
              ),
              SizedBox(
                height: 20,
              ),
              SvgPicture.asset(
                "images/senme.svg",
                height: 38.25,
              ),
              Padding(
                padding: EdgeInsets.only(top: 40),
                child: Text(
                  'Равным образом новая модель деятельности представляет собой интересный эксперимент проверки дальнейших направлений развития. Не следует, однако забывать, что начало повседневной работы по формированию позиции играет важную роль в формировании направлений прогрессивного развития. Разнообразный и богатый опыт постоянное информационно-пропагандистское обеспечение нашей деятельности требуют определения и уточнения систем массового участия. С другой стороны укрепление и развитие структуры способствует подготовки и реализации форм развития. Товарищи! консультация с широким активом обеспечивает широкому кругу (специалистов) участие в формировании новых предложений. ',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      fontWeight: FontWeight.w400),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
