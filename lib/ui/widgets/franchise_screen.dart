import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:senme/core/config_core.dart';

class FranchiseScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Франшиза Senme'),
        centerTitle: true,
      ),
      body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          color: ConfigCore.colorPrimary,
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  width: 204,
                  height: 172,
                  child: Stack(
                    children: [
                      Align(
                        alignment: Alignment.center,
                        child: SvgPicture.asset("images/full_heart.svg"),
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: SvgPicture.asset("images/qr.svg"),
                      )
                    ],
                  ),
                ),
                SvgPicture.asset(
                  "images/senme.svg",
                  color: Colors.white,
                  height: 60.25,
                ),
              ],
            ),
          )),
    );
  }
}
