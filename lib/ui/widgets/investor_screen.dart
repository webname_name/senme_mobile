import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class InvestorScreen extends StatelessWidget {
  final text =
      'ТОО «ВИКА Казахстан»\nРНН 600300125196\nБИН 020140007102\nр/с а тенге KZ89826A1KZTD2004583\n(в стандарте IBAN)\nАО АТФ Банк г. Алматы\nБИК  ALMNKZKA\nДиректор – Арынова С.С.\n\nАдрес: 050036,  г.Алматы \nм-рн 1, БЦ Болашак\nтел.: +7 727 220 80 08\ne-mail: info@wika.kz\nwebsite: www.wika.com\n\n№ счета (IBAN)\nТ KZ89826A1KZTD2004583\n\n€  KZ47826A1USDD2000918\n\n\$ KZ37826A1EURD2000367';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Инвесторам'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          margin: EdgeInsets.only(top: 114, left: 21, right: 21),
          child: Column(
            children: [
              SvgPicture.asset(
                "images/senme_logo.svg",
                height: 57.84,
              ),
              Padding(
                padding: EdgeInsets.only(top: 40),
                child: Text(
                  text,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      fontWeight: FontWeight.w400),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
