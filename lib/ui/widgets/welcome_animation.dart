import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:senme/ui/images/pulsing_heart.dart';
import 'package:senme/wellcome/welcome_bloc.dart';
import 'package:senme/wellcome/welcome_event.dart';

class WelcomeAnimationScreen extends StatefulWidget {
  final double screenWidth;
  final double screenHeight;

  const WelcomeAnimationScreen(
      {Key key, @required this.screenWidth, @required this.screenHeight})
      : super(key: key);
  @override
  State<StatefulWidget> createState() => WelcomeAnimationScreenState();
}

class WelcomeAnimationScreenState extends State<WelcomeAnimationScreen>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation<double> _animation;

  @override
  void initState() {
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 360));
    _animation =
        CurvedAnimation(parent: _animationController, curve: Curves.linear);

    _animationController.addStatusListener((status) {
      if (_animationController.status == AnimationStatus.completed) {
        Future.delayed(
            Duration(milliseconds: 150),
            () => BlocProvider.of<WelcomeBloc>(context)
                .add(WelcomeEventInitialFinished()));
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print('${widget.screenWidth}');
    Future.delayed(
        Duration(
          seconds: 2,
        ),
        () => _animationController.forward());
    return Stack(
      children: <Widget>[
        Align(
          alignment: Alignment.center,
          child: PulsingHeart(
            duration: Duration(
              seconds: 1,
            ),
            isAutoHide: true,
          ),
        ),
        ScaleTransition(
          scale: _animation,
          child: Align(
            alignment: Alignment.center,
            child: Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("images/white_heart.png"),
                      fit: BoxFit.cover)),
              child: Stack(
                children: [
                  Align(
                    alignment: Alignment.center,
                    child: Image.asset('images/scroll_cards.png'),
                  ),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }
}
