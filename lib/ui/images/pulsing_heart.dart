import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:senme/ui/images/pulsing_svg_picture.dart';

class PulsingHeart extends StatefulWidget {
  final Duration duration;
  final bool isAutoHide;

  const PulsingHeart(
      {Key key, @required this.duration, @required this.isAutoHide})
      : super(key: key);
  @override
  State<StatefulWidget> createState() => _PulsingHeart();
}

class _PulsingHeart extends State<PulsingHeart>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation _animationWidth;
  Animation _animationHeight;

  @override
  void initState() {
    _animationController =
        AnimationController(vsync: this, duration: widget.duration);
    _animationWidth =
        Tween(begin: 0.0, end: 200.0).animate(_animationController);
    _animationHeight =
        Tween(begin: 0.0, end: 200.0).animate(_animationController);

    _animationController.addStatusListener((status) {
      if (widget.isAutoHide) {
        if (status == AnimationStatus.completed) {
          Future.delayed(Duration(seconds: 1500),
              () => _animationController.reverse());
        }
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _animationController.forward();
    return AnimatedBuilder(
        animation: _animationWidth,
        builder: (context, _) {
          return Container(
            color: Colors.transparent,
            width: _animationWidth.value,
            height: _animationHeight.value,
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.center,
                  child: PulsingSvgPicture(
                    assetSvgName: 'images/purple_heart_4.svg',
                    duration: Duration(milliseconds: 700),
                    startWidth: 146.99,
                    startHeight: 115.81,
                    endWidth: 146.99 + 146.99 * 0.2,
                    endHeight: 115.81 + 115.81 * 0.2,
                  ),
                ),
                Align(
                  alignment: Alignment.center,
                  child: PulsingSvgPicture(
                    assetSvgName: 'images/purple_heart_3.svg',
                    duration: Duration(milliseconds: 550),
                    startWidth: 145.5,
                    startHeight: 114.19,
                    endWidth: 145.5 + 145.5 * 0.2,
                    endHeight: 114.19 + 114.19 * 0.2,
                  ),
                ),
                Align(
                  alignment: Alignment.center,
                  child: PulsingSvgPicture(
                    assetSvgName: 'images/purple_heart_2.svg',
                    duration: Duration(milliseconds: 600),
                    startWidth: 135.21,
                    startHeight: 100.45,
                    endWidth: 135.21 + 135.21 * 0.2,
                    endHeight: 100.45 + 100.45 * 0.2,
                  ),
                ),
                Align(
                  alignment: Alignment.center,
                  child: PulsingSvgPicture(
                    assetSvgName: 'images/purple_heart.svg',
                    duration: Duration(milliseconds: 700),
                    startWidth: 119.34,
                    startHeight: 99.36,
                    endWidth: 119.34 + 119.34 * 0.2,
                    endHeight: 99.36 + 99.36 * 0.2,
                  ),
                ),
                Align(
                  alignment: Alignment.center,
                  child: SvgPicture.asset(
                    'images/heart.svg',
                    color: Colors.white,
                    width: 67.62,
                    height: 56.89,
                  ),
                )
              ],
            ),
          );
        });
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }
}
