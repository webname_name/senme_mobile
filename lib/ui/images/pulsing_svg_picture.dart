import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class PulsingSvgPicture extends StatefulWidget {
  final double startWidth;
  final double endWidth;
  final double startHeight;
  final double endHeight;
  final Duration duration;
  final String assetSvgName;

  const PulsingSvgPicture(
      {Key key,
      this.startWidth,
      this.endWidth,
      this.startHeight,
      this.endHeight,
      this.duration,
      this.assetSvgName})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _PulsingSvgPictureState();
}

class _PulsingSvgPictureState extends State<PulsingSvgPicture>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation _animationWidth;
  Animation _animationHeight;

  @override
  void initState() {
    _animationController =
        AnimationController(vsync: this, duration: widget.duration);
    _animationWidth = Tween(begin: widget.startWidth, end: widget.endWidth)
        .animate(CurvedAnimation(
            curve: Curves.easeOut, parent: _animationController));
    _animationHeight = Tween(begin: widget.startHeight, end: widget.endHeight)
        .animate(CurvedAnimation(
            curve: Curves.easeOut, parent: _animationController));
    _animationController.repeat(reverse: true);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: _animationWidth,
        builder: (context, _) {
          return SvgPicture.asset(
            widget.assetSvgName,
            width: _animationWidth.value,
            height: _animationHeight.value,
          );
        });
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }
}
