import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:senme/core/config_core.dart';

class DialogBuilder {
  static void infoDialog(BuildContext context, String text,
      {String title = ConfigCore.appName,
      String buttonText = 'Закрыть',
      Function function}) {
    showDialog<void>(
      context: context,
      barrierDismissible: false,
      // false = user must tap button, true = tap outside dialog
      builder: (BuildContext dialogContext) {
        return AlertDialog(
          title: Text(title),
          content: Text(text),
          actions: <Widget>[
            FlatButton(
              child: Text(buttonText),
              onPressed: () {
                Navigator.of(dialogContext).pop();
                if (function != null) {
                  function.call();
                }
              },
            ),
          ],
        );
      },
    );
  }

  static void confirmDialog(BuildContext context, String text,
      {String title = ConfigCore.appName,
      String cancelText = 'Отменить',
      String confirmText = 'Подтвердить',
      Function onConfirm}) {
    showDialog(
      context: context,
      barrierDismissible: false,
      // false = user must tap button, true = tap outside dialog
      builder: (BuildContext dialogContext) {
        return AlertDialog(
          title: Text(title),
          content: Text(text),
          actions: <Widget>[
            FlatButton(
              child: Text(cancelText),
              onPressed: () {
                Navigator.of(dialogContext).pop();
              },
            ),
            FlatButton(
              child: Text(confirmText),
              onPressed: () {
                Navigator.of(dialogContext).pop();
                if (onConfirm != null) {
                  onConfirm.call();
                }
              },
            ),
          ],
        );
      },
    );
  }

  static void showThankDialogOld(BuildContext _context, Function function) {
    showDialog<void>(
      context: _context,
      barrierDismissible: false,
      // false = user must tap button, true = tap outside dialog
      builder: (BuildContext dialogContext) {
        return Dialog(
          child: Container(
            width: 290,
            height: 290,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Stack(
                  children: [
                    Container(
                      width: 290,
                      height: 150,
                      child: SvgPicture.asset(
                        "images/grey_rounded_background.svg",
                        fit: BoxFit.fill,
                      ),
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Padding(
                        padding: EdgeInsets.only(top: 27),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SvgPicture.asset("images/gift.svg"),
                            Text(
                              ConfigCore.appName,
                              style: TextStyle(
                                  color: ConfigCore.colorPrimaryColor,
                                  fontSize: 40,
                                  fontWeight: FontWeight.w700),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Спасибо!',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.w600),
                ),
                SizedBox(
                  height: 15,
                ),
                Text(
                    'Мы рады, что Вы с нами.\nОтправляйте открытки и подарки\nв любую точку мира',
                    style: TextStyle(color: Color(0xff828282), fontSize: 12)),
                FlatButton(
                  child: Text(
                    'Закрыть',
                    style: TextStyle(color: ConfigCore.colorPrimaryColor),
                  ),
                  onPressed: () {
                    Navigator.pop(dialogContext);
                    if (function != null) {
                      function.call();
                    }
                  },
                )
              ],
            ),
          ),
        );
      },
    );
  }

  static void showThankDialog(BuildContext _context, Function function) {
    showDialog<void>(
      context: _context,
      barrierDismissible: false,
      // false = user must tap button, true = tap outside dialog
      builder: (BuildContext dialogContext) {
        return Dialog(
          child: Container(
            width: double.infinity,
            height: 357,
            color: ConfigCore.colorPrimary,
            child: Stack(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 119),
                  child: Wrap(
                    runSpacing: 20,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(left: 66, right: 66),
                        child: SvgPicture.asset(
                          "images/senme_logo.svg",
                          color: Colors.white,
                          height: 57.84,
                        ),
                      ),
                      Center(
                        child: Column(
                          children: [
                            Text(
                              'Добро пожаловать,',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500),
                            ),
                            Text(
                              'в мир покупок Senme!',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: IconButton(
                      icon: Icon(
                        Icons.close,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        Navigator.pop(dialogContext);
                        if (function != null) {
                          function.call();
                        }
                      }),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  static void showThankDialogPoints(
      BuildContext _context, Function function, int points) {
    showDialog<void>(
      context: _context,
      barrierDismissible: false,
      // false = user must tap button, true = tap outside dialog
      builder: (BuildContext dialogContext) {
        return Dialog(
          child: Container(
            width: double.infinity,
            height: 372,
            color: ConfigCore.colorPrimary,
            child: Stack(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 60),
                  child: Wrap(
                    runSpacing: 20,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(left: 66, right: 66),
                        child: SvgPicture.asset(
                          "images/senme_logo.svg",
                          color: Colors.white,
                          height: 57.84,
                        ),
                      ),
                      Center(
                        child: Column(
                          children: [
                            if (points != null)
                              Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        'Мы дарим Вам:',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      Text(
                                        '$points баллов',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 20,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(
                                            top: 31, bottom: 31),
                                        width: 53.5,
                                        height: 2,
                                        color: Colors.black,
                                      )
                                    ],
                                  )
                                ],
                              ),
                            Text(
                              'Добро пожаловать,',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500),
                            ),
                            Text(
                              'в мир покупок Senme!',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: IconButton(
                      icon: Icon(
                        Icons.close,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        Navigator.pop(dialogContext);
                        if (function != null) {
                          function.call();
                        }
                      }),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  static void franchiseDialog(BuildContext context) {
    showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (_context) => Dialog(
              child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  color: ConfigCore.colorPrimary,
                  child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          width: 204,
                          height: 172,
                          child: Stack(
                            children: [
                              Align(
                                alignment: Alignment.center,
                                child:
                                    SvgPicture.asset("images/full_heart.svg"),
                              ),
                              Align(
                                alignment: Alignment.center,
                                child: SvgPicture.asset("images/qr.svg"),
                              )
                            ],
                          ),
                        ),
                        SvgPicture.asset(
                          "images/senme.svg",
                          color: Colors.white,
                          height: 60.25,
                        ),
                      ],
                    ),
                  )),
            ));
  }
}
