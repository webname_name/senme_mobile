import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:senme/core/config_core.dart';

class RedButton extends StatelessWidget {
  final String text;
  final Function function;

  const RedButton({Key key, @required this.text, @required this.function})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      minWidth: 150,
      height: 40,
      onPressed: () {
        if (function != null) {
          function.call();
        }
      },
      child: Text(
        text,
        style: TextStyle(
            color: Colors.white, fontSize: 12, fontWeight: FontWeight.w700),
      ),
      color: ConfigCore.colorPrimaryColor,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
    );
  }
}
