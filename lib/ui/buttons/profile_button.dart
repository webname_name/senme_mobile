import 'package:flutter/material.dart';

class ProfileButton extends StatelessWidget {
  final String text;
  final Function function;
  const ProfileButton({
    Key key,
    this.text,
    this.function,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 40,
      child: RaisedButton(
        onPressed: () {
          if (function != null) {
            function.call();
          }
        },
        color: Colors.white,
        child: Text(
          text,
          style: TextStyle(
              color: Color(0xff0A1034),
              fontSize: 14,
              fontWeight: FontWeight.w600),
        ),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(4),
            side: BorderSide(
                color: Color(0xffE0E0E0), width: 1, style: BorderStyle.solid)),
      ),
    );
  }
}
