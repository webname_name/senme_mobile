import 'package:flutter/material.dart';

class NotAuthorizeProfileButton extends StatelessWidget {
  final String text;
  final Function function;

  const NotAuthorizeProfileButton(
      {Key key, @required this.text, @required this.function})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: () {
        if (function != null) {
          function.call();
        }
      },
      color: Colors.white,
      child: Text(
        text,
        style: TextStyle(
            color: Color(0xff8A8A8A),
            fontSize: 12,
            fontWeight: FontWeight.w500),
      ),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(4),
          side: BorderSide(
              color: Color(0xffE0E0E0), width: 1, style: BorderStyle.solid)),
    );
  }
}
