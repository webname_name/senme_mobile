import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class GeneralProgressButton extends StatelessWidget {
  final String text;
  final Color backgroundColor;
  final Color textColor;
  final double fontSize;
  final Function function;
  final bool isShowProgress;

  const GeneralProgressButton(
      {Key key,
      this.text,
      this.backgroundColor = Colors.white,
      this.textColor = Colors.black,
      this.fontSize = 16,
      this.function,
      this.isShowProgress = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      minWidth: 150,
      height: 50,
      onPressed: () {
        if (function != null) {
          function.call();
        }
      },
      child: Stack(
        children: [
          Align(
            alignment: Alignment.center,
            child: Text(
              text,
              style: TextStyle(
                  color: textColor,
                  fontSize: fontSize,
                  fontWeight: FontWeight.w700),
            ),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Container(
              width: 25,
              height: 25,
              child: isShowProgress
                  ? CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                    )
                  : null,
            ),
          )
        ],
      ),
      color: backgroundColor,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
    );
  }
}
