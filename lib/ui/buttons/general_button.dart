import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class GeneralButton extends StatelessWidget {
  final String text;
  final Color backgroundColor;
  final Color textColor;
  final double fontSize;
  final Function function;

  const GeneralButton(
      {Key key,
      this.text,
      this.backgroundColor = Colors.white,
      this.textColor = Colors.black,
      this.fontSize = 16,
      this.function})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      minWidth: 150,
      height: 50,
      onPressed: () {
        if (function != null) {
          function.call();
        }
      },
      child: Text(
        text,
        style: TextStyle(
            color: textColor, fontSize: fontSize, fontWeight: FontWeight.w700),
      ),
      color: backgroundColor,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
    );
  }
}
