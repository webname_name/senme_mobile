import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class PasswordField extends StatefulWidget {
  final String labelText;
  final TextEditingController textEditingController;
  final FocusNode focusNode;
  final Function validator;
  const PasswordField(
      {Key key,
      @required this.labelText,
      @required this.textEditingController,
      @required this.focusNode,
      @required this.validator})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _PasswordFieldState();
}

class _PasswordFieldState extends State<PasswordField> {
  var isObscureText = true;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration: InputDecoration(
          labelText: widget.labelText,
          enabledBorder: const OutlineInputBorder(
              borderSide:
                  const BorderSide(color: Color(0xffD0C9D6), width: 1.5)),
          // border: const OutlineInputBorder(borderSide: const BorderSide(color: Color(0xffD0C9D6), width: 1.5)),
          focusedBorder: const OutlineInputBorder(
              borderSide:
                  const BorderSide(color: Color(0xff2F80ED), width: 1.5)),
          errorBorder: const OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.red, width: 1.5)),
          focusedErrorBorder: const OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.red, width: 1.5)),
          errorStyle: TextStyle(color: Colors.red),
          prefixIcon: Icon(Icons.lock),
          suffixIcon: IconButton(
            icon: Icon(isObscureText ? Icons.visibility_off : Icons.visibility),
            onPressed: () => {setState(() => isObscureText = !isObscureText)},
          )),
      controller: widget.textEditingController,
      obscureText: isObscureText,
      autovalidate: true,
      focusNode: widget.focusNode,
      validator: (value) => widget.validator(value),
    );
  }
}
