import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class PhoneNumberFieldNew extends StatefulWidget {
  final String labelText;
  final String hintText;
  final TextEditingController textEditingController;
  final FocusNode focusNode;
  final Function validator;

  final phoneMaskFormatter;

  const PhoneNumberFieldNew(
      {Key key,
      @required this.labelText,
      @required this.hintText,
      @required this.textEditingController,
      @required this.focusNode,
      @required this.validator,
      @required this.phoneMaskFormatter})
      : super(key: key);
  @override
  State<StatefulWidget> createState() => _PhoneNumberState();
}

class _PhoneNumberState extends State<PhoneNumberFieldNew> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration: InputDecoration(
          labelText: widget.labelText,
          hintText: widget.hintText,
          labelStyle: TextStyle(fontWeight: FontWeight.w500),
          hintStyle: TextStyle(fontWeight: FontWeight.w500),
          enabledBorder: const OutlineInputBorder(
              borderSide:
                  const BorderSide(color: Color(0xffD0C9D6), width: 1.5),
              borderRadius: BorderRadius.all(const Radius.circular(10))),
          // border: const OutlineInputBorder(borderSide: const BorderSide(color: Color(0xffD0C9D6), width: 1.5)),
          focusedBorder: const OutlineInputBorder(
              borderSide:
                  const BorderSide(color: Color(0xff2F80ED), width: 1.5),
              borderRadius: BorderRadius.all(const Radius.circular(10))),
          errorBorder: const OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.red, width: 1.5),
              borderRadius: BorderRadius.all(const Radius.circular(10))),
          focusedErrorBorder: const OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.red, width: 1.5),
              borderRadius: BorderRadius.all(const Radius.circular(10))),
          errorStyle: TextStyle(color: Colors.red),
          prefixIcon: Icon(Icons.phone)),
      keyboardType: TextInputType.phone,
      controller: widget.textEditingController,
      obscureText: false,
      autovalidate: false,
      validator: (value) => widget.validator(value),
      inputFormatters: [widget.phoneMaskFormatter],
    );
  }
}
