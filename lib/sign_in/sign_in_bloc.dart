import 'dart:io';

import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:senme/api/api_service.dart';
import 'package:senme/dto/response_service.dart';
import 'package:senme/dto/token.dart';
import 'package:senme/repository/user_repository.dart';
import 'package:senme/sign_in/sign_in_event.dart';
import 'package:senme/sign_in/sign_in_state.dart';

class SignInBloc extends Bloc<SignInEvent, SignInState> {
  bool isFormValid = false;
  SignInBloc() : super(SignInStateInitial());

  var _apiService = new ApiService();
  var _userRepository = new UserRepository();

  final formKey = GlobalKey<FormState>();
  var usernameController = TextEditingController();
  var passwordController = TextEditingController();
  var phoneMaskFormatter = new MaskTextInputFormatter(
      mask: '+# (###) ###-##-##', filter: {"#": RegExp(r'[0-9]')});

  @override
  Stream<SignInState> mapEventToState(SignInEvent event) async* {
    if (event is SignInEventRun) {
      yield SignInStateLoading(true);
      ResponseService responseService = await _apiService.getToken(
          usernameController.text, passwordController.text);
      if (responseService.statusCode == null ||
          responseService.statusCode == HttpStatus.badRequest) {
        yield SignInStateFailure(responseService.data);
      } else if (responseService.statusCode == HttpStatus.unauthorized) {
        yield SignInStateFailure('Логин или пароль не верны');
      } else {
        Token token = responseService.data;
        _userRepository.persistToken(token.toJson().toString());
        yield SignInStateSuccess();
      }
    }
    if (event is SignInFormStatusChanged) {
      isFormValid = event.isValid;
      yield SignInStateInitial(isFormValid: isFormValid);
    }
  }
}
