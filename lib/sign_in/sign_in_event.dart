import 'package:equatable/equatable.dart';

abstract class SignInEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class SignInFormStatusChanged extends SignInEvent {
  final bool isValid;
  SignInFormStatusChanged(this.isValid);

  @override
  List<Object> get props => [isValid];
}

class SignInEventRun extends SignInEvent {}
