import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:senme/core/config_core.dart';
import 'package:senme/core/utils.dart';
import 'package:senme/sign_in/sign_in_bloc.dart';
import 'package:senme/sign_in/sign_in_event.dart';
import 'package:senme/sign_in/sign_in_state.dart';
import 'package:senme/sign_in_or_sign_up/signIn_or_signup_event.dart';
import 'package:senme/sign_in_or_sign_up/sign_in_sign_up_bloc.dart';
import 'package:senme/ui/buttons/red_button.dart';
import 'package:senme/ui/dialogs/dialog_builder.dart';
import 'package:senme/ui/fields/password_field.dart';
import 'package:senme/ui/fields/phone_number_field.dart';

class SignInScreenOld extends StatelessWidget {
  final double statusBarHeight;
  final String username;
  final String password;
  bool isAutoLogin;
  SignInScreenOld(
      {Key key,
      @required this.statusBarHeight,
      this.username,
      this.password,
      this.isAutoLogin = false})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => SignInBloc(),
      child: BlocListener<SignInBloc, SignInState>(
        listener: (context, state) {
          if (state is SignInStateFailure) {
            DialogBuilder.infoDialog(context, state.message);
          } else if (state is SignInStateSuccess) {
            DialogBuilder.showThankDialog(context, () {
              Navigator.pop(context);
              context.bloc<SignInSignUpBloc>().add(SignInEventSuccess());
            });
          }
        },
        child: BlocBuilder<SignInBloc, SignInState>(
          builder: (context, state) {
            print('isAutoLogin loop~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
            if (isAutoLogin) {
              if (username != null) {
                context.bloc<SignInBloc>().usernameController.text = username;
              }
              if (password != null) {
                context.bloc<SignInBloc>().passwordController.text = password;
              }
              isAutoLogin = false;
              context.bloc<SignInBloc>().add(SignInEventRun());
            }
            return Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Stack(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 417,
                    child: SvgPicture.asset(
                      "images/grey_rounded_background.svg",
                      fit: BoxFit.fill,
                    ),
                  ),
                  Align(
                    alignment: Alignment.topCenter,
                    child: Padding(
                      padding: EdgeInsets.only(top: 53 - 24.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SvgPicture.asset("images/gift.svg"),
                          Text(
                            ConfigCore.appName,
                            style: TextStyle(
                                color: ConfigCore.colorPrimaryColor,
                                fontSize: 40,
                                fontWeight: FontWeight.w700),
                          ),
                          Form(
                              key: context.bloc<SignInBloc>().formKey,
                              child: Padding(
                                padding: EdgeInsets.all(5),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(top: 15),
                                      child: SizedBox(
                                        width: 280,
                                        child: PhoneNumberField(
                                          labelText: 'Номер телефона*',
                                          hintText: '+x(xxx) xxx-xx-xx',
                                          textEditingController: context
                                              .bloc<SignInBloc>()
                                              .usernameController,
                                          validator: (value) => Utils
                                                  .isPhoneNumberValid(value)
                                              ? null
                                              : "Введите номер по образцу +x(xxx) xxx-xx-xx",
                                          phoneMaskFormatter: context
                                              .bloc<SignInBloc>()
                                              .phoneMaskFormatter,
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(top: 15),
                                      child: SizedBox(
                                        width: 280,
                                        height: 60,
                                        child: PasswordField(
                                            labelText: 'Пароль',
                                            textEditingController: context
                                                .bloc<SignInBloc>()
                                                .passwordController,
                                            validator: (value) => value.length <
                                                    6
                                                ? "Минимальная длина пароля 6 символов"
                                                : null),
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(top: 20),
                                      child: RedButton(
                                          text: 'Вход',
                                          function: () {
                                            if (!context
                                                .bloc<SignInBloc>()
                                                .formKey
                                                .currentState
                                                .validate()) {
                                              return;
                                            }
                                            context
                                                .bloc<SignInBloc>()
                                                .add(SignInEventRun());
                                          }),
                                    ),
                                    Padding(
                                        padding: EdgeInsets.only(top: 5),
                                        child: InkWell(
                                          onTap: () {
                                            context
                                                .bloc<SignInSignUpBloc>()
                                                .add(ChangeViewMode());
                                          },
                                          child: Text(
                                            'Ещё нет аккаунта? Зарегистрируйтесь',
                                            style: TextStyle(
                                                color: Color(0xff2F80ED),
                                                fontSize: 12,
                                                decoration:
                                                    TextDecoration.underline),
                                          ),
                                        )),
                                    Padding(
                                      padding: EdgeInsets.only(top: 15),
                                      child: Container(
                                        padding: EdgeInsets.only(top: 10),
                                        child: state is SignInStateLoading &&
                                                state.isLoading
                                            ? CircularProgressIndicator(
                                                backgroundColor: Colors.white12,
                                                valueColor:
                                                    AlwaysStoppedAnimation<
                                                            Color>(
                                                        ConfigCore
                                                            .colorPrimaryColor),
                                              )
                                            : null,
                                      ),
                                    )
                                  ],
                                ),
                              ))
                        ],
                      ),
                    ),
                  ),
                  /*Align(
                    alignment:Alignment.center,
                    child: Container(
                      padding: EdgeInsets.only(top: 10),
                      child: state is SignInStateLoading && state.isLoading
                          ? CircularProgressIndicator(
                        backgroundColor: Colors.white12,
                        valueColor: AlwaysStoppedAnimation<Color>(
                            ConfigCore.colorPrimaryColor),
                      )
                          : null,
                    ),
                  )*/
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
