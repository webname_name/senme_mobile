import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:senme/core/config_core.dart';
import 'package:senme/core/utils.dart';
import 'package:senme/sign_in/sign_in_bloc.dart';
import 'package:senme/sign_in/sign_in_event.dart';
import 'package:senme/sign_in/sign_in_state.dart';
import 'package:senme/sign_in_or_sign_up/signIn_or_signup_event.dart';
import 'package:senme/sign_in_or_sign_up/sign_in_sign_up_bloc.dart';
import 'package:senme/ui/buttons/general_progress_button.dart';
import 'package:senme/ui/dialogs/dialog_builder.dart';
import 'package:senme/ui/fields/password_field_new.dart';
import 'package:senme/ui/fields/phone_number_field_new.dart';

class SignInScreen extends StatelessWidget {
  final String username;
  final String password;
  bool isAutoLogin;
  SignInScreen(
      {Key key, this.username, this.password, this.isAutoLogin = false})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => SignInBloc(),
      child: BlocListener<SignInBloc, SignInState>(
        listener: (context, state) {
          if (state is SignInStateFailure) {
            DialogBuilder.infoDialog(context, state.message);
          } else if (state is SignInStateSuccess) {
            isAutoLogin
                ? DialogBuilder.showThankDialogPoints(context, () {
                    context.bloc<SignInSignUpBloc>().add(SignInEventSuccess());
                  }, 2000)
                : DialogBuilder.showThankDialog(context, () {
                    context.bloc<SignInSignUpBloc>().add(SignInEventSuccess());
                  });
          }
        },
        child: BlocBuilder<SignInBloc, SignInState>(
          builder: (context, state) {
            print('isAutoLogin loop~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
            if (isAutoLogin) {
              if (username != null) {
                context.bloc<SignInBloc>().usernameController.text = username;
              }
              if (password != null) {
                context.bloc<SignInBloc>().passwordController.text = password;
              }
              isAutoLogin = false;
              context.bloc<SignInBloc>().add(SignInEventRun());
            }
            return Container(
              width: MediaQuery.of(context).size.width,
              child: Stack(
                children: [
                  Align(
                    alignment: Alignment.topCenter,
                    child: Padding(
                      padding: EdgeInsets.only(top: 145 - kToolbarHeight),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SvgPicture.asset(
                            "images/senme_logo.svg",
                            height: 57.84,
                          ),
                          Form(
                              key: context.bloc<SignInBloc>().formKey,
                              onChanged: () {
                                var isValid = context
                                    .bloc<SignInBloc>()
                                    .formKey
                                    .currentState
                                    .validate();
                                context
                                    .bloc<SignInBloc>()
                                    .add(SignInFormStatusChanged(isValid));
                              },
                              child: Padding(
                                padding: EdgeInsets.only(
                                    left: 21,
                                    right: 21,
                                    top: 119.88 - kToolbarHeight),
                                child: Wrap(
                                  runSpacing: 20,
                                  children: [
                                    SizedBox(
                                      width: double.infinity,
                                      child: PhoneNumberFieldNew(
                                        labelText: 'Номер телефона*',
                                        hintText: '+x(xxx) xxx-xx-xx',
                                        textEditingController: context
                                            .bloc<SignInBloc>()
                                            .usernameController,
                                        validator: (value) => Utils
                                                .isPhoneNumberValid(value)
                                            ? null
                                            : "Введите номер по образцу +x(xxx) xxx-xx-xx",
                                        phoneMaskFormatter: context
                                            .bloc<SignInBloc>()
                                            .phoneMaskFormatter,
                                      ),
                                    ),
                                    SizedBox(
                                      width: double.infinity,
                                      child: PasswordFieldNew(
                                          labelText: 'Пароль',
                                          textEditingController: context
                                              .bloc<SignInBloc>()
                                              .passwordController,
                                          validator: (value) => value.length < 6
                                              ? "Минимальная длина пароля 6 символов"
                                              : null),
                                    ),
                                    SizedBox(
                                      width: double.infinity,
                                      height: 50,
                                      child: GeneralProgressButton(
                                          text: 'Войти',
                                          backgroundColor: context
                                                  .bloc<SignInBloc>()
                                                  .isFormValid
                                              ? ConfigCore.colorPrimary
                                              : Color(0xffCCCDCF),
                                          textColor: Colors.white,
                                          isShowProgress:
                                              state is SignInStateLoading &&
                                                  state.isLoading,
                                          function: () {
                                            if (!context
                                                .bloc<SignInBloc>()
                                                .formKey
                                                .currentState
                                                .validate()) {
                                              return;
                                            }
                                            context
                                                .bloc<SignInBloc>()
                                                .add(SignInEventRun());
                                          }),
                                    ),
                                    InkWell(
                                        onTap: () {
                                          context
                                              .bloc<SignInSignUpBloc>()
                                              .add(ChangeViewMode());
                                        },
                                        child: Row(
                                          mainAxisSize: MainAxisSize.max,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              'Ещё нет аккаунта?',
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w700),
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            Text(
                                              'Зарегистрируйтесь',
                                              style: TextStyle(
                                                  color:
                                                      ConfigCore.colorPrimary,
                                                  fontSize: 16,
                                                  decoration:
                                                      TextDecoration.underline,
                                                  fontWeight: FontWeight.w700),
                                            ),
                                          ],
                                        )),
                                    /*Container(
                                      alignment: Alignment.center,
                                      child: state is SignInStateLoading &&
                                              state.isLoading
                                          ? CircularProgressIndicator(
                                              backgroundColor: Colors.white12,
                                              valueColor:
                                                  AlwaysStoppedAnimation<Color>(
                                                      ConfigCore.primaryColor),
                                            )
                                          : null,
                                    )*/
                                  ],
                                ),
                              ))
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
