import 'package:equatable/equatable.dart';

abstract class SignInState extends Equatable {
  @override
  List<Object> get props => [];
}

class SignInStateInitial extends SignInState {
  final bool isFormValid;
  SignInStateInitial({this.isFormValid = false});

  @override
  List<Object> get props => [isFormValid];
}

class SignInStateLoading extends SignInState {
  final bool isLoading;
  SignInStateLoading(this.isLoading);
  @override
  List<Object> get props => [isLoading];
}

class SignInStateFailure extends SignInState {
  final String message;

  SignInStateFailure(this.message);
  @override
  List<Object> get props => [message];
}

class SignInStateSuccess extends SignInState {}
