import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:senme/repository/user_repository.dart';

import 'auth_event.dart';
import 'auth_state.dart';

class AuthBloc extends Bloc<AuthenticationEvent, AuthenticationState> {
  AuthBloc() : super(null);
  UserRepository userRepository = new UserRepository();
  bool hasToken = false;

  @override
  AuthenticationState get initialState => getInitialState();

  getInitialState() {
    return AppStartedState();
  }

  @override
  Stream<AuthenticationState> mapEventToState(
      AuthenticationEvent event) async* {
    print('AuthBloc mapEventToState>>>>>>>>>>>>>>>>>>');
    if (event is AppStarted) {
      hasToken = await userRepository.hasToken();
      if (hasToken) {
//        yield AuthenticationState(AuthenticationType.AuthenticationAuthenticated);
        yield AuthenticationAuthenticated();
      } else {
//        yield AuthenticationState(AuthenticationType.AuthenticationUnauthenticated);
        yield AuthenticationUnauthenticated();
      }
    }

    if (event is LoggedIn) {
//      yield AuthenticationState(AuthenticationType.AuthenticationLoading);
      yield AuthenticationLoading();
      await userRepository.persistToken(event.token);
      yield AuthenticationAuthenticated();
//      yield AuthenticationState(AuthenticationType.AuthenticationAuthenticated);
    }

    if (event is LoggedOut) {
//      yield AuthenticationState(AuthenticationType.AuthenticationLoading);
      yield AuthenticationLoading();
      await userRepository.deleteAllData();
//      yield AuthenticationState(AuthenticationType.AuthenticationUnauthenticated);
      hasToken = false;
      yield AuthenticationUnauthenticated();
    }
  }
}
