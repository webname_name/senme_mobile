import 'package:equatable/equatable.dart';

class AuthenticationState extends Equatable {
//  final AuthenticationType type;
//
//  AuthenticationState(this.type);

  @override
  List<Object> get props => [];
}

class AppStartedState extends AuthenticationState {}

class AuthenticationUninitialized extends AuthenticationState {}

class AuthenticationAuthenticated extends AuthenticationState {}

class AuthenticationUnauthenticated extends AuthenticationState {}

class AuthenticationLoading extends AuthenticationState {}

enum AuthenticationType {
  AuthenticationUninitialized,
  AuthenticationAuthenticated,
  AuthenticationUnauthenticated,
  AuthenticationLoading
}
