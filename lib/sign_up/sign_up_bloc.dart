import 'dart:io';

import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:senme/api/api_service.dart';
import 'package:senme/dto/response_service.dart';
import 'package:senme/dto/user_registration_form.dart';
import 'package:senme/sign_up/sign_up_event.dart';
import 'package:senme/sign_up/sign_up_state.dart';

class SignUpBloc extends Bloc<SignUpEvent, SignUpState> {
  bool isFormValid = false;

  SignUpBloc() : super(SignUpStateInitial());

  var _apiService = new ApiService();

  final formKey = GlobalKey<FormState>();
  var usernameController = TextEditingController();
  var phoneController = TextEditingController();
  var emailController = TextEditingController();
  var passwordController = TextEditingController();
  var passwordConfirmController = TextEditingController();
  var phoneMaskFormatter = new MaskTextInputFormatter(
      mask: '+# (###) ###-##-##', filter: {"#": RegExp(r'[0-9]')});

  @override
  Stream<SignUpState> mapEventToState(SignUpEvent event) async* {
    if (event is SignUpEventRun) {
      yield SignUpStateLoading(true);
      String email = emailController.text.trim();
      ResponseService responseService = await _apiService.registration(
          UserRegistrationForm(
              usernameController.text.trim(),
              phoneController.text.trim(),
              email.isNotEmpty ? null : email,
              passwordController.text.trim()));
      if (responseService.statusCode == null ||
          responseService.statusCode == HttpStatus.badRequest) {
        yield SignUpStateFailure(responseService.data);
      } else {
        yield SignUpStateSuccess();
      }
    } else if (event is SignUpFormStatusChanged) {
      isFormValid = event.isValid;
      yield SignUpStateInitial(isFormValid: isFormValid);
    }
  }
}
