import 'package:equatable/equatable.dart';

abstract class SignUpEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class SignUpFormStatusChanged extends SignUpEvent {
  final bool isValid;
  SignUpFormStatusChanged(this.isValid);

  @override
  List<Object> get props => [isValid];
}

class SignUpEventRun extends SignUpEvent {}
