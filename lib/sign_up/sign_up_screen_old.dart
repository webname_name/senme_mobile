import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:senme/core/config_core.dart';
import 'package:senme/core/utils.dart';
import 'package:senme/sign_in_or_sign_up/signIn_or_signup_event.dart';
import 'package:senme/sign_in_or_sign_up/sign_in_sign_up_bloc.dart';
import 'package:senme/sign_up/sign_up_bloc.dart';
import 'package:senme/sign_up/sign_up_event.dart';
import 'package:senme/sign_up/sign_up_state.dart';
import 'package:senme/ui/buttons/red_button.dart';
import 'package:senme/ui/dialogs/dialog_builder.dart';
import 'package:senme/ui/fields/password_field.dart';
import 'package:senme/ui/fields/phone_number_field.dart';

class SignUpScreenOld extends StatelessWidget {
  final double statusBarHeight;
  const SignUpScreenOld({Key key, @required this.statusBarHeight})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => SignUpBloc(),
      child: BlocListener<SignUpBloc, SignUpState>(
        listener: (context, state) {
          if (state is SignUpStateFailure) {
            DialogBuilder.infoDialog(context, state.message);
          } else if (state is SignUpStateSuccess) {
            context.bloc<SignInSignUpBloc>().add(ChangeViewMode(
                username: context.bloc<SignUpBloc>().phoneController.text,
                password: context.bloc<SignUpBloc>().passwordController.text,
                isAutoLogin: true));
          }
        },
        child: BlocBuilder<SignUpBloc, SignUpState>(
          builder: (context, state) {
            return Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Stack(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 555.07,
                    child: SvgPicture.asset(
                      "images/grey_rounded_background.svg",
                      fit: BoxFit.fill,
                    ),
                  ),
                  Align(
                    alignment: Alignment.topCenter,
                    child: Padding(
                      padding: EdgeInsets.only(top: (53.0 - statusBarHeight)),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SvgPicture.asset("images/gift.svg"),
                          Text(
                            ConfigCore.appName,
                            style: TextStyle(
                                color: ConfigCore.colorPrimaryColor,
                                fontSize: 40,
                                fontWeight: FontWeight.w700),
                          ),
                          Text('Зарегистрируйтесь и\nполучите баллы на покупку',
                              style: TextStyle(
                                  color: Color(0xff404040),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600)),
                          Form(
                              key: context.bloc<SignUpBloc>().formKey,
                              child: Padding(
                                padding: EdgeInsets.all(5),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SizedBox(
                                      width: 280,
                                      child: TextFormField(
                                        decoration: InputDecoration(
                                          labelText: 'Ваше имя*',
                                          enabledBorder:
                                              const OutlineInputBorder(
                                                  borderSide: const BorderSide(
                                                      color: Color(0xffD0C9D6),
                                                      width: 1.5)),
                                          // border: const OutlineInputBorder(borderSide: const BorderSide(color: Color(0xffD0C9D6), width: 1.5)),
                                          focusedBorder:
                                              const OutlineInputBorder(
                                                  borderSide: const BorderSide(
                                                      color: Color(0xff2F80ED),
                                                      width: 1.5)),
                                          errorBorder: const OutlineInputBorder(
                                              borderSide: const BorderSide(
                                                  color: Colors.red,
                                                  width: 1.5)),
                                          focusedErrorBorder:
                                              const OutlineInputBorder(
                                                  borderSide: const BorderSide(
                                                      color: Colors.red,
                                                      width: 1.5)),
                                          errorStyle:
                                              TextStyle(color: Colors.red),
                                          prefixIcon:
                                              Icon(Icons.account_circle),
                                        ),
                                        controller: context
                                            .bloc<SignUpBloc>()
                                            .usernameController,
                                        obscureText: false,
                                        autovalidate: true,
                                        validator: (value) => value.isEmpty
                                            ? 'Имя обязательное поле'
                                            : null,
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(top: 5),
                                      child: SizedBox(
                                        width: 280,
                                        child: PhoneNumberField(
                                          labelText: 'Номер телефона*',
                                          hintText: '+x(xxx) xxx-xx-xx',
                                          textEditingController: context
                                              .bloc<SignUpBloc>()
                                              .phoneController,
                                          validator: (value) => Utils
                                                  .isPhoneNumberValid(value)
                                              ? null
                                              : "Введите номер по образцу +x(xxx) xxx-xx-xx",
                                          phoneMaskFormatter: context
                                              .bloc<SignUpBloc>()
                                              .phoneMaskFormatter,
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(top: 5),
                                      child: SizedBox(
                                        width: 280,
                                        child: TextFormField(
                                          decoration: InputDecoration(
                                              labelText: 'Email',
                                              enabledBorder:
                                                  const OutlineInputBorder(
                                                      borderSide:
                                                          const BorderSide(
                                                              color: Color(
                                                                  0xffD0C9D6),
                                                              width: 1.5)),
                                              // border: const OutlineInputBorder(borderSide: const BorderSide(color: Color(0xffD0C9D6), width: 1.5)),
                                              focusedBorder:
                                                  const OutlineInputBorder(
                                                      borderSide:
                                                          const BorderSide(
                                                              color: Color(
                                                                  0xff2F80ED),
                                                              width: 1.5)),
                                              errorBorder:
                                                  const OutlineInputBorder(
                                                      borderSide:
                                                          const BorderSide(
                                                              color: Colors.red,
                                                              width: 1.5)),
                                              focusedErrorBorder:
                                                  const OutlineInputBorder(
                                                      borderSide:
                                                          const BorderSide(
                                                              color: Colors.red,
                                                              width: 1.5)),
                                              errorStyle:
                                                  TextStyle(color: Colors.red),
                                              prefixIcon: Icon(Icons.email)),
                                          keyboardType:
                                              TextInputType.emailAddress,
                                          controller: context
                                              .bloc<SignUpBloc>()
                                              .emailController,
                                          obscureText: false,
                                          autovalidate: true,
                                          validator: (value) => value.isEmpty ||
                                                  Utils.isEmailValid(value)
                                              ? null
                                              : 'Email имеет некорректный формат',
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(top: 5),
                                      child: SizedBox(
                                        width: 280,
                                        height: 60,
                                        child: PasswordField(
                                            labelText: 'Пароль',
                                            textEditingController: context
                                                .bloc<SignUpBloc>()
                                                .passwordController,
                                            validator: (value) => value.length <
                                                    6
                                                ? "Минимальная длина пароля 6 символов"
                                                : null),
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(top: 5),
                                      child: SizedBox(
                                        width: 280,
                                        child: PasswordField(
                                            labelText: 'Повторите пароль',
                                            textEditingController: context
                                                .bloc<SignUpBloc>()
                                                .passwordConfirmController,
                                            validator: (value) => value !=
                                                    context
                                                        .bloc<SignUpBloc>()
                                                        .passwordController
                                                        .text
                                                ? "Пароли не совпадают"
                                                : null),
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(top: 20),
                                      child: RedButton(
                                        text: 'Продолжить',
                                        function: () {
                                          if (!context
                                              .bloc<SignUpBloc>()
                                              .formKey
                                              .currentState
                                              .validate()) {
                                            return;
                                          }
                                          context
                                              .bloc<SignUpBloc>()
                                              .add(SignUpEventRun());
                                        },
                                      ),
                                    ),
                                    Padding(
                                        padding: EdgeInsets.only(top: 5),
                                        child: InkWell(
                                          onTap: () => context
                                              .bloc<SignInSignUpBloc>()
                                              .add(ChangeViewMode()),
                                          child: Text(
                                            'Уже есть аккаунт? Войти',
                                            style: TextStyle(
                                                color: Color(0xff2F80ED),
                                                fontSize: 12,
                                                decoration:
                                                    TextDecoration.underline),
                                          ),
                                        )),
                                  ],
                                ),
                              ))
                        ],
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Container(
                      child: state is SignUpStateLoading && state.isLoading
                          ? CircularProgressIndicator(
                              backgroundColor: Colors.white12,
                              valueColor: AlwaysStoppedAnimation<Color>(
                                  ConfigCore.colorPrimaryColor),
                            )
                          : null,
                    ),
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
