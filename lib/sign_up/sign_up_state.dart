import 'package:equatable/equatable.dart';

abstract class SignUpState extends Equatable {
  @override
  List<Object> get props => [];
}

class SignUpStateInitial extends SignUpState {
  final bool isFormValid;
  SignUpStateInitial({this.isFormValid = false});

  @override
  List<Object> get props => [isFormValid];
}

class SignUpStateLoading extends SignUpState {
  final bool isLoading;
  SignUpStateLoading(this.isLoading);
  @override
  List<Object> get props => [isLoading];
}

class SignUpStateFailure extends SignUpState {
  final String message;

  SignUpStateFailure(this.message);
  @override
  List<Object> get props => [message];
}

class SignUpStateSuccess extends SignUpState {}
