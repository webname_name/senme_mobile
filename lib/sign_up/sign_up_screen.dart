import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:senme/core/config_core.dart';
import 'package:senme/core/utils.dart';
import 'package:senme/sign_in_or_sign_up/signIn_or_signup_event.dart';
import 'package:senme/sign_in_or_sign_up/sign_in_sign_up_bloc.dart';
import 'package:senme/sign_up/sign_up_bloc.dart';
import 'package:senme/sign_up/sign_up_event.dart';
import 'package:senme/sign_up/sign_up_state.dart';
import 'package:senme/ui/buttons/general_progress_button.dart';
import 'package:senme/ui/dialogs/dialog_builder.dart';
import 'package:senme/ui/fields/password_field_new.dart';
import 'package:senme/ui/fields/phone_number_field_new.dart';

class SignUpScreen extends StatelessWidget {
  const SignUpScreen({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => SignUpBloc(),
      child: BlocListener<SignUpBloc, SignUpState>(
        listener: (context, state) {
          if (state is SignUpStateFailure) {
            DialogBuilder.infoDialog(context, state.message);
          } else if (state is SignUpStateSuccess) {
            context.bloc<SignInSignUpBloc>().add(ChangeViewMode(
                username: context.bloc<SignUpBloc>().phoneController.text,
                password: context.bloc<SignUpBloc>().passwordController.text,
                isAutoLogin: true));
          }
        },
        child: BlocBuilder<SignUpBloc, SignUpState>(
          builder: (context, state) {
            return Container(
              width: MediaQuery.of(context).size.width,
              child: Stack(
                children: [
                  Align(
                    alignment: Alignment.topCenter,
                    child: Padding(
                      padding: EdgeInsets.only(left: 21, right: 21, top: 40),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SvgPicture.asset(
                            "images/senme_logo.svg",
                            height: 57.84,
                          ),
                          Form(
                              key: context.bloc<SignUpBloc>().formKey,
                              onChanged: () {
                                var isValid = context
                                    .bloc<SignUpBloc>()
                                    .formKey
                                    .currentState
                                    .validate();
                                context
                                    .bloc<SignUpBloc>()
                                    .add(SignUpFormStatusChanged(isValid));
                              },
                              child: Padding(
                                padding: EdgeInsets.only(top: 103.88),
                                child: Wrap(
                                  runSpacing: 20,
                                  children: [
                                    SizedBox(
                                      width: double.infinity,
                                      child: TextFormField(
                                        decoration: InputDecoration(
                                          labelText: 'Ваше имя*',
                                          labelStyle: TextStyle(
                                              fontWeight: FontWeight.w500),
                                          enabledBorder:
                                              const OutlineInputBorder(
                                                  borderSide: const BorderSide(
                                                      color: Color(0xffD0C9D6),
                                                      width: 1.5),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          const Radius.circular(
                                                              10))),
                                          // border: const OutlineInputBorder(borderSide: const BorderSide(color: Color(0xffD0C9D6), width: 1.5)),
                                          focusedBorder:
                                              const OutlineInputBorder(
                                                  borderSide: const BorderSide(
                                                      color: Color(0xff2F80ED),
                                                      width: 1.5),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          const Radius.circular(
                                                              10))),
                                          errorBorder: const OutlineInputBorder(
                                              borderSide: const BorderSide(
                                                  color: Colors.red,
                                                  width: 1.5),
                                              borderRadius: BorderRadius.all(
                                                  const Radius.circular(10))),
                                          focusedErrorBorder:
                                              const OutlineInputBorder(
                                                  borderSide: const BorderSide(
                                                      color: Colors.red,
                                                      width: 1.5),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          const Radius.circular(
                                                              10))),
                                          errorStyle:
                                              TextStyle(color: Colors.red),
                                          prefixIcon:
                                              Icon(Icons.account_circle),
                                        ),
                                        controller: context
                                            .bloc<SignUpBloc>()
                                            .usernameController,
                                        obscureText: false,
                                        validator: (value) => value.isEmpty
                                            ? 'Имя обязательное поле'
                                            : null,
                                      ),
                                    ),
                                    SizedBox(
                                      width: double.infinity,
                                      child: PhoneNumberFieldNew(
                                        labelText: 'Номер телефона*',
                                        hintText: '+x(xxx) xxx-xx-xx',
                                        textEditingController: context
                                            .bloc<SignUpBloc>()
                                            .phoneController,
                                        validator: (value) => Utils
                                                .isPhoneNumberValid(value)
                                            ? null
                                            : "Введите номер по образцу +x(xxx) xxx-xx-xx",
                                        phoneMaskFormatter: context
                                            .bloc<SignUpBloc>()
                                            .phoneMaskFormatter,
                                      ),
                                    ),
                                    SizedBox(
                                      width: double.infinity,
                                      child: TextFormField(
                                        decoration: InputDecoration(
                                            labelText: 'Email',
                                            labelStyle: TextStyle(
                                                fontWeight: FontWeight.w500),
                                            enabledBorder: const OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                    color: Color(0xffD0C9D6),
                                                    width: 1.5),
                                                borderRadius: BorderRadius.all(
                                                    const Radius.circular(10))),
                                            // border: const OutlineInputBorder(borderSide: const BorderSide(color: Color(0xffD0C9D6), width: 1.5)),
                                            focusedBorder: const OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                    color: Color(0xff2F80ED),
                                                    width: 1.5),
                                                borderRadius: BorderRadius.all(
                                                    const Radius.circular(10))),
                                            errorBorder: const OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                    color: Colors.red,
                                                    width: 1.5),
                                                borderRadius: BorderRadius.all(
                                                    const Radius.circular(10))),
                                            focusedErrorBorder:
                                                const OutlineInputBorder(
                                                    borderSide: const BorderSide(
                                                        color: Colors.red,
                                                        width: 1.5),
                                                    borderRadius:
                                                        BorderRadius.all(const Radius.circular(10))),
                                            errorStyle: TextStyle(color: Colors.red),
                                            prefixIcon: Icon(Icons.email)),
                                        keyboardType:
                                            TextInputType.emailAddress,
                                        controller: context
                                            .bloc<SignUpBloc>()
                                            .emailController,
                                        obscureText: false,
                                        validator: (value) => value.isEmpty ||
                                                Utils.isEmailValid(value)
                                            ? null
                                            : 'Email имеет некорректный формат',
                                      ),
                                    ),
                                    SizedBox(
                                      width: double.infinity,
                                      child: PasswordFieldNew(
                                          labelText: 'Пароль',
                                          textEditingController: context
                                              .bloc<SignUpBloc>()
                                              .passwordController,
                                          validator: (value) => value.length < 6
                                              ? "Минимальная длина пароля 6 символов"
                                              : null),
                                    ),
                                    SizedBox(
                                      width: double.infinity,
                                      child: PasswordFieldNew(
                                          labelText: 'Повторите пароль',
                                          textEditingController: context
                                              .bloc<SignUpBloc>()
                                              .passwordConfirmController,
                                          validator: (value) => value !=
                                                  context
                                                      .bloc<SignUpBloc>()
                                                      .passwordController
                                                      .text
                                              ? "Пароли не совпадают"
                                              : null),
                                    ),
                                    SizedBox(
                                      width: double.infinity,
                                      height: 50,
                                      child: GeneralProgressButton(
                                          text: 'Регистрация',
                                          backgroundColor: context
                                                  .bloc<SignUpBloc>()
                                                  .isFormValid
                                              ? ConfigCore.colorPrimary
                                              : Color(0xffCCCDCF),
                                          textColor: Colors.white,
                                          isShowProgress:
                                              state is SignUpStateLoading &&
                                                  state.isLoading,
                                          function: () {
                                            if (!context
                                                .bloc<SignUpBloc>()
                                                .formKey
                                                .currentState
                                                .validate()) {
                                              return;
                                            }
                                            context
                                                .bloc<SignUpBloc>()
                                                .add(SignUpEventRun());
                                          }),
                                    ),
                                    InkWell(
                                        onTap: () {
                                          context
                                              .bloc<SignInSignUpBloc>()
                                              .add(ChangeViewMode());
                                        },
                                        child: Row(
                                          mainAxisSize: MainAxisSize.max,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              'Уже есть аккаунт?',
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w700),
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            Text(
                                              'Войти',
                                              style: TextStyle(
                                                  color:
                                                      ConfigCore.colorPrimary,
                                                  fontSize: 16,
                                                  decoration:
                                                      TextDecoration.underline,
                                                  fontWeight: FontWeight.w700),
                                            ),
                                          ],
                                        ))
                                  ],
                                ),
                              ))
                        ],
                      ),
                    ),
                  ),
                  /*Align(
                    alignment: Alignment.center,
                    child: Container(
                      child: state is SignUpStateLoading && state.isLoading
                          ? CircularProgressIndicator(
                              backgroundColor: Colors.white12,
                              valueColor: AlwaysStoppedAnimation<Color>(
                                  ConfigCore.primaryColor),
                            )
                          : null,
                    ),
                  )*/
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
