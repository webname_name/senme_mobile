import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:senme/core/config_core.dart';
import 'package:senme/mainscreen/main_screen_bloc.dart';
import 'package:senme/profile/profile_bloc.dart';
import 'package:senme/profile/profile_state.dart';
import 'package:senme/sign_in_or_sign_up/signIn_or_signup_page_old.dart';
import 'package:senme/sign_in_or_sign_up/signIn_or_signup_state.dart';
import 'package:senme/sign_in_or_sign_up/sign_in_sign_up_bloc.dart';
import 'package:senme/ui/buttons/not_authorize_profile_button.dart';
import 'package:senme/ui/buttons/red_button.dart';

class NotAuthorizeWidgetOld extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ProfileBloc profileBloc = BlocProvider.of<ProfileBloc>(context);
    print('NotAuthorizeWidget profileBloc: $profileBloc');
    print('mainBloc: ${context.bloc<MainScreenBloc>().state}');
    return BlocBuilder<ProfileBloc, ProfileState>(
        builder: (context, state) => SingleChildScrollView(
              child: Stack(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 310.07,
                    child: SvgPicture.asset(
                      "images/grey_rounded_background.svg",
                      fit: BoxFit.fill,
                    ),
                  ),
                  Align(
                    alignment: Alignment.topCenter,
                    child: Padding(
                      padding: EdgeInsets.only(top: 36),
                      child: SvgPicture.asset("images/gift.svg"),
                    ),
                  ),
                  Align(
                    alignment: Alignment.topCenter,
                    child: Padding(
                      padding: EdgeInsets.only(top: 99),
                      child: Text(
                        ConfigCore.appName,
                        style: TextStyle(
                            color: ConfigCore.colorPrimaryColor,
                            fontSize: 40,
                            fontWeight: FontWeight.w700),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 174, left: 25, right: 25),
                    padding: EdgeInsets.all(10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Зарегистрируйтесь и\nполучите баллы на покупку',
                              style: TextStyle(
                                  color: Color(0xff828282), fontSize: 10),
                            ),
                            RedButton(
                              text: 'Зарегистрироваться',
                              function: () {
                                Navigator.push(context,
                                    MaterialPageRoute(builder: (context) {
                                  return BlocProvider(
                                    create: (context) => SignInSignUpBloc(
                                        SignInOrSignUpState.initial()
                                            .copyWith(isSignInPage: false),
                                        profileBloc),
                                    child: SignInOrSignUpPageOld(),
                                  );
                                }));
                              },
                            )
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  '1000',
                                  style: TextStyle(
                                      color: ConfigCore.colorPrimaryColor,
                                      fontSize: 30,
                                      fontWeight: FontWeight.w700),
                                ),
                                Text(
                                  'балов',
                                  style: TextStyle(
                                      color: ConfigCore.colorPrimaryColor),
                                ),
                              ],
                            ),
                            RedButton(
                                text: 'Войти',
                                function: () {
                                  Navigator.push(context,
                                      MaterialPageRoute(builder: (context) {
                                    return BlocProvider(
                                      create: (context) => SignInSignUpBloc(
                                          SignInOrSignUpState.initial(),
                                          profileBloc),
                                      child: SignInOrSignUpPageOld(),
                                    );
                                  }));
                                })
                          ],
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 320, left: 25, right: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                          width: double.infinity,
                          child: RaisedButton.icon(
                            onPressed: () => {},
                            color: Colors.white,
                            icon: SvgPicture.asset("images/help_service.svg"),
                            label: Text(
                              'Служба поддержки',
                              style: TextStyle(
                                  color: Color(0xff8A8A8A),
                                  fontSize: 12,
                                  fontWeight: FontWeight.w500),
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(4),
                                side: BorderSide(
                                    color: Color(0xffE0E0E0),
                                    width: 1,
                                    style: BorderStyle.solid)),
                          ),
                        ),
                        SizedBox(
                          width: double.infinity,
                          child: NotAuthorizeProfileButton(
                            text: 'О приложении',
                            function: () => {},
                          ),
                        ),
                        SizedBox(
                          width: double.infinity,
                          child: RaisedButton(
                            padding: EdgeInsets.only(top: 13, bottom: 13),
                            onPressed: () => {},
                            color: ConfigCore.colorPrimaryColor,
                            child: Text(
                              'Франшиза senme\nОткрой свой бизнес\nсвяжись с нами',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w500),
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(4),
                                side: BorderSide(
                                    color: Color(0xffE0E0E0),
                                    width: 1,
                                    style: BorderStyle.solid)),
                          ),
                        ),
                        SizedBox(
                          width: double.infinity,
                          child: NotAuthorizeProfileButton(
                            text: 'Инвесторам',
                            function: () => {},
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ));
  }
}
