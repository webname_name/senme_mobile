import 'package:equatable/equatable.dart';

class ProfileEvent extends Equatable {
  final EAction action;

  ProfileEvent(this.action);
  @override
  List<Object> get props => [action];
}

enum EAction { INIT, LOGIN, LOGOUT }
