import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:senme/auth/auth_bloc.dart';
import 'package:senme/core/config_core.dart';
import 'package:senme/core/utils.dart';
import 'package:senme/profile/profile_bloc.dart';
import 'package:senme/profile/profile_event.dart';
import 'package:senme/profile/profile_state.dart';
import 'package:senme/sign_in_or_sign_up/signIn_or_signup_page.dart';
import 'package:senme/sign_in_or_sign_up/signIn_or_signup_state.dart';
import 'package:senme/sign_in_or_sign_up/sign_in_sign_up_bloc.dart';
import 'package:senme/ui/buttons/general_button.dart';
import 'package:senme/ui/dialogs/dialog_builder.dart';
import 'package:senme/ui/widgets/about_app_screen.dart';
import 'package:senme/ui/widgets/franchise_screen.dart';
import 'package:senme/ui/widgets/investor_screen.dart';

class ProfileWidget extends StatelessWidget {
  final String title;

  const ProfileWidget({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print('ProfileWidget build>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
    return BlocProvider<ProfileBloc>(
      create: (context) => ProfileBloc(null, context.bloc<AuthBloc>())
        ..add(ProfileEvent(EAction.INIT)),
      child: BlocListener<ProfileBloc, ProfileState>(
        listener: (context, state) {
          print(
              'ProfileWidget state>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>${state.mode}');
        },
        child: BlocBuilder<ProfileBloc, ProfileState>(
          builder: (context, state) {
            print(
                'ProfileWidget authBloc hasToken: ${context.bloc<AuthBloc>().hasToken}');
            if (state != null) {
              // if(state.mode==EMode.SIGN_IN_SIGN_UP){
              //   return NotAuthorizeWidget();
              // }else{
              //   return ProfileDetails();
              // }
              return ProfileDetails();
            }
            return Container(width: 0.0, height: 0.0);
          },
        ),
      ),
    );
  }
}

/*class ProfileDetailsOld extends StatelessWidget {
  const ProfileDetailsOld({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      color: Color(0xffE5E5E5),
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(top: 30, left: 17, right: 17),
          child: Column(
            children: [
              Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Container(
                    alignment: Alignment.bottomLeft,
                    height: 50,
                    child: Text(
                      '${context.bloc<ProfileBloc>().points}',
                      style: TextStyle(
                          color: ConfigCore.colorPrimaryColor,
                          fontSize: 40,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Container(
                    alignment: Alignment.bottomLeft,
                    height: 50,
                    child: Padding(
                      padding: EdgeInsets.only(bottom: 7),
                      child: Text(
                        'баллов',
                        style: TextStyle(
                            color: ConfigCore.colorPrimaryColor,
                            fontSize: 20,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                  )
                ],
              ),
              Padding(
                  padding: EdgeInsets.only(top: 10),
                  child: ProfileButton(
                    text: 'Мои карты',
                    function: () => {},
                  )),
              Padding(
                  padding: EdgeInsets.only(top: 10),
                  child: ProfileButton(
                    text: 'Служба поддержки',
                    function: () => {},
                  )),
              Padding(
                  padding: EdgeInsets.only(top: 10),
                  child: ProfileButton(
                    text: 'О приложении',
                    function: () => {},
                  )),
              Padding(
                  padding: EdgeInsets.only(top: 10),
                  child: ProfileButton(
                    text: 'Франшиза SENME',
                    function: () => {},
                  )),
              Padding(
                  padding: EdgeInsets.only(top: 10),
                  child: ProfileButton(
                    text: 'Инвесторам',
                    function: () => {},
                  )),
              Padding(
                padding: EdgeInsets.only(top: 10),
                child: ProfileButton(
                  text: 'Выйти из аккаунта',
                  function: () => DialogBuilder.confirmDialog(
                      context, 'Вы уверены, что хотите выйти из аккаунта?',
                      onConfirm: () async {
                    await context
                        .bloc<ProfileBloc>()
                        .authBloc
                        .userRepository
                        .deleteToken();
                    context
                        .bloc<ProfileBloc>()
                        .add(ProfileEvent(EAction.LOGOUT));
                  }),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}*/

class ProfileDetails extends StatelessWidget {
  const ProfileDetails({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ProfileBloc profileBloc = BlocProvider.of<ProfileBloc>(context);
    return BlocBuilder<ProfileBloc, ProfileState>(
        builder: (context, state) => SingleChildScrollView(
              child: Container(
                width: MediaQuery.of(context).size.width,
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.topCenter,
                      child: Padding(
                        padding: EdgeInsets.only(top: 100 - kToolbarHeight),
                        child: SvgPicture.asset(
                          "images/senme_logo.svg",
                          height: 57.84,
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          top: 200 - kToolbarHeight,
                          left: 21,
                          right: 21,
                          bottom: 51),
                      child: Wrap(
                        runSpacing: 24,
                        children: [
                          Align(
                            alignment: Alignment.center,
                            child: Column(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text('${context.bloc<ProfileBloc>().points}',
                                    style: TextStyle(
                                        color:
                                            context.bloc<ProfileBloc>().points >
                                                    0
                                                ? ConfigCore.colorPrimary
                                                : Color(0xffe6e6e8),
                                        fontSize: 60,
                                        fontWeight: FontWeight.w700)),
                                Text('баллов',
                                    style: TextStyle(
                                        color: Color(0xff98989c),
                                        fontSize: 20,
                                        fontWeight: FontWeight.w400))
                              ],
                            ),
                          ),
                          if (state.mode == EMode.SIGN_IN_SIGN_UP)
                            SizedBox(
                              width: double.infinity,
                              height: 50,
                              child: GeneralButton(
                                text: ' Вход / Регистрация',
                                textColor: Color(0xffEA152F),
                                function: () => {
                                  Navigator.push(context,
                                      MaterialPageRoute(builder: (context) {
                                    return BlocProvider<SignInSignUpBloc>(
                                      create: (context) => SignInSignUpBloc(
                                          SignInOrSignUpState.initial(),
                                          profileBloc),
                                      child: SignInOrSignUpPage(),
                                    );
                                  }))
                                },
                              ),
                            ),
                          if (state.mode == EMode.PROFILE)
                            SizedBox(
                              width: double.infinity,
                              height: 50,
                              child: GeneralButton(
                                text: 'Выйти из аккаунта',
                                function: () => DialogBuilder.confirmDialog(
                                    context,
                                    'Вы уверены, что хотите выйти из аккаунта?',
                                    onConfirm: () async {
                                  await context
                                      .bloc<ProfileBloc>()
                                      .authBloc
                                      .userRepository
                                      .deleteToken();
                                  context
                                      .bloc<ProfileBloc>()
                                      .add(ProfileEvent(EAction.LOGOUT));
                                }),
                              ),
                            ),
                          if (state.mode == EMode.PROFILE)
                            SizedBox(
                              width: double.infinity,
                              height: 50,
                              child: GeneralButton(
                                  text: 'Мои карты', function: () {}),
                            ),
                          SizedBox(
                            width: double.infinity,
                            height: 50,
                            child: GeneralButton(
                              text: 'Служба поддержки',
                              function: () => {
                                Utils.launchInBrowser(
                                    'https://telegram.im/@GenroGuardBot')
                              },
                            ),
                          ),
                          SizedBox(
                            width: double.infinity,
                            height: 50,
                            child: GeneralButton(
                              text: 'О приложении',
                              function: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AboutAppScreen())),
                            ),
                          ),
                          SizedBox(
                            width: double.infinity,
                            height: 50,
                            child: GeneralButton(
                              text: 'Инвесторам',
                              function: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => InvestorScreen())),
                            ),
                          ),
                          SizedBox(
                            width: double.infinity,
                            height: 50,
                            child: RaisedButton(
                              padding: EdgeInsets.only(top: 13, bottom: 13),
                              // onPressed: () =>DialogBuilder.franchiseDialog(context),
                              onPressed: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => FranchiseScreen())),
                              color: Color(0xffe91630),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    'Франшиза Senme',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w700),
                                  ),
                                  SizedBox(
                                    width: 9,
                                  ),
                                  SvgPicture.asset(
                                    "images/info.svg",
                                    width: 11,
                                    height: 11,
                                  )
                                ],
                              ),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  side: BorderSide(
                                      color: Color(0xffE0E0E0),
                                      width: 1,
                                      style: BorderStyle.solid)),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ));
  }
}
