import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:senme/auth/auth_bloc.dart';
import 'package:senme/profile/profile_event.dart';
import 'package:senme/profile/profile_state.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  final AuthBloc authBloc;
  int points = 0;
  ProfileBloc(ProfileState initialState, this.authBloc) : super(initialState);
  @override
  Stream<ProfileState> mapEventToState(ProfileEvent event) async* {
    switch (event.action) {
      case EAction.INIT:
        {
          if (authBloc.hasToken) {
            points = 1000; //TODO get from API
            yield ProfileState(EMode.PROFILE);
          } else {
            points = 0;
            yield ProfileState(EMode.SIGN_IN_SIGN_UP);
          }
          break;
        }
      case EAction.LOGOUT:
        authBloc.hasToken = false;
        points = 0;
        yield ProfileState(EMode.SIGN_IN_SIGN_UP);
        break;
      case EAction.LOGIN:
        points = 1000; //TODO get from API
        yield ProfileState(EMode.PROFILE);
        break;
    }
  }
}
