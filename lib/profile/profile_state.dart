import 'package:equatable/equatable.dart';

class ProfileState extends Equatable {
  EMode mode;

  ProfileState(this.mode);

  @override
  List<Object> get props => [mode];
}

enum EMode { PROFILE, SIGN_IN_SIGN_UP }
