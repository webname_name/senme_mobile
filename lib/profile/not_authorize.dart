import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:senme/mainscreen/main_screen_bloc.dart';
import 'package:senme/profile/profile_bloc.dart';
import 'package:senme/profile/profile_state.dart';
import 'package:senme/sign_in_or_sign_up/signIn_or_signup_page.dart';
import 'package:senme/sign_in_or_sign_up/signIn_or_signup_state.dart';
import 'package:senme/sign_in_or_sign_up/sign_in_sign_up_bloc.dart';
import 'package:senme/ui/buttons/general_button.dart';

class NotAuthorizeWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ProfileBloc profileBloc = BlocProvider.of<ProfileBloc>(context);
    print('NotAuthorizeWidget profileBloc: $profileBloc');
    print('mainBloc: ${context.bloc<MainScreenBloc>().state}');
    return BlocBuilder<ProfileBloc, ProfileState>(
        builder: (context, state) => SingleChildScrollView(
              child: Stack(
                children: [
                  Align(
                    alignment: Alignment.topCenter,
                    child: Padding(
                      padding: EdgeInsets.only(top: 100 - kToolbarHeight),
                      child: SvgPicture.asset(
                        "images/senme_logo.svg",
                        height: 57.84,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        top: 200 - kToolbarHeight, left: 21, right: 21),
                    child: Wrap(
                      runSpacing: 24,
                      children: [
                        Align(
                          alignment: Alignment.center,
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text('0',
                                  style: TextStyle(
                                      color: Color(0xffe6e6e8),
                                      fontSize: 60,
                                      fontWeight: FontWeight.w700)),
                              Text('баллов',
                                  style: TextStyle(
                                      color: Color(0xff98989c),
                                      fontSize: 20,
                                      fontWeight: FontWeight.w400))
                            ],
                          ),
                        ),
                        SizedBox(
                          width: double.infinity,
                          height: 50,
                          child: GeneralButton(
                            text: ' Вход / Регистрация',
                            textColor: Color(0xffEA152F),
                            function: () => {
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) {
                                return BlocProvider(
                                  create: (context) => SignInSignUpBloc(
                                      SignInOrSignUpState.initial(),
                                      profileBloc),
                                  child: SignInOrSignUpPage(),
                                );
                              }))
                            },
                          ),
                        ),
                        SizedBox(
                          width: double.infinity,
                          height: 50,
                          child: GeneralButton(
                            text: 'Служба поддержки',
                            function: () => {},
                          ),
                        ),
                        SizedBox(
                          width: double.infinity,
                          height: 50,
                          child: GeneralButton(
                            text: 'О приложении',
                            function: () => {},
                          ),
                        ),
                        SizedBox(
                          width: double.infinity,
                          height: 50,
                          child: GeneralButton(
                            text: 'Инвесторам',
                            function: () => {},
                          ),
                        ),
                        SizedBox(
                          width: double.infinity,
                          height: 50,
                          child: RaisedButton(
                            padding: EdgeInsets.only(top: 13, bottom: 13),
                            onPressed: () => {},
                            color: Color(0xffe91630),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'Франшиза Senme',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w700),
                                ),
                                SizedBox(
                                  width: 9,
                                ),
                                SvgPicture.asset(
                                  "images/info.svg",
                                  width: 11,
                                  height: 11,
                                )
                              ],
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                                side: BorderSide(
                                    color: Color(0xffE0E0E0),
                                    width: 1,
                                    style: BorderStyle.solid)),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ));
  }
}
