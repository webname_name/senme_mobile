import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:senme/core/config_core.dart';
import 'package:senme/timer/timer_bloc.dart';
import 'package:senme/timer/timer_event.dart';
import 'package:senme/timer/timer_state.dart';

class TimerWidget extends StatelessWidget {
  final DateTime expiredDate;
  final Function callback;

  const TimerWidget({Key key, this.expiredDate, this.callback})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    print('build>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
    Timer timer;
    return BlocProvider(
      create: (context) {
        return TimerBloc(initialState: TimerState(), expiredDate: expiredDate)
          ..add(TimerEvent(status: EStatus.init));
      },
      child: BlocListener<TimerBloc, TimerState>(
        listener: (context, state) {
          if (timer != null && state.seconds == 0) {
            timer.cancel();
            if (callback != null) {
              callback.call();
            }
          }
        },
        child: BlocBuilder<TimerBloc, TimerState>(
          builder: (context, state) {
            if (state.seconds > 0) {
              TimerBloc _timerBloc = BlocProvider.of<TimerBloc>(context);
              timer = Timer(
                  Duration(seconds: 1),
                  () => BlocProvider.of<TimerBloc>(context)
                      .add(TimerEvent(status: EStatus.init)));
            }
            return Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: 40,
                  height: 36,
                  margin: EdgeInsets.only(left: 10),
                  color: Color(0xffEFEFEF),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '${state.days}',
                        style: TextStyle(color: Colors.black, fontSize: 16),
                      ),
                      Text(
                        'дн',
                        style: TextStyle(color: Colors.black, fontSize: 12),
                      )
                    ],
                  ),
                ),
                Container(
                  width: 40,
                  height: 36,
                  margin: EdgeInsets.only(left: 10),
                  color: Color(0xffEFEFEF),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '${state.hours}',
                        style: TextStyle(color: Colors.black, fontSize: 16),
                      ),
                      Text(
                        'час',
                        style: TextStyle(color: Colors.black, fontSize: 12),
                      )
                    ],
                  ),
                ),
                Container(
                  width: 40,
                  height: 36,
                  margin: EdgeInsets.only(left: 10),
                  color: Color(0xffEFEFEF),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '${state.minutes}',
                        style: TextStyle(color: Colors.black, fontSize: 16),
                      ),
                      Text(
                        'мин',
                        style: TextStyle(color: Colors.black, fontSize: 12),
                      )
                    ],
                  ),
                ),
                Container(
                  width: 40,
                  height: 36,
                  margin: EdgeInsets.only(left: 10),
                  color: Color(0xffEFEFEF),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '${state.seconds}',
                        style: TextStyle(color: Colors.black, fontSize: 16),
                      ),
                      Text(
                        'сек',
                        style: TextStyle(color: Colors.black, fontSize: 12),
                      )
                    ],
                  ),
                )
              ],
            );
          },
        ),
      ),
    );
  }
}

class TimeItem extends StatelessWidget {
  const TimeItem({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TimerBloc _bloc = BlocProvider.of<TimerBloc>(context);
    return Container(
      width: 36,
      height: 36,
      color: ConfigCore.greyColor,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            '${_bloc.state.days}',
            style: TextStyle(color: Colors.black, fontSize: 16),
          ),
          Text(
            'дн',
            style: TextStyle(color: Colors.black, fontSize: 12),
          )
        ],
      ),
    );
  }
}
