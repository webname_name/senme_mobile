import 'package:equatable/equatable.dart';

class TimerState extends Equatable {
  final int days;
  final int hours;
  final int minutes;
  final int seconds;

  @override
  List<Object> get props => [days, hours, minutes, seconds];

  TimerState(
      {this.days = 0, this.hours = 0, this.minutes = 0, this.seconds = 0});
}
