import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:senme/timer/timer_event.dart';
import 'package:senme/timer/timer_state.dart';

class TimerBloc extends Bloc<TimerEvent, TimerState> {
  final DateTime expiredDate;
  TimerBloc({TimerState initialState, this.expiredDate}) : super(initialState);

  @override
  Stream<TimerState> mapEventToState(TimerEvent event) async* {
    switch (event.status) {
      case EStatus.init:
        {
          yield await updateTimer();
          break;
        }
      case EStatus.start:
        {
          break;
        }
      case EStatus.stop:
        {
          break;
        }
    }
  }

  Future<TimerState> updateTimer() async {
    Duration duration = expiredDate.difference(DateTime.now());
    int days = duration.inDays;
    int hours = duration.inHours - duration.inDays * 24;
    int minutes = duration.inMinutes - days * 24 * 60 - hours * 60;
    int seconds = duration.inSeconds -
        days * 24 * 60 * 60 -
        hours * 60 * 60 -
        minutes * 60;
    return TimerState(
        days: days, hours: hours, minutes: minutes, seconds: seconds);
  }

  Stream<TimerState> handleTimeout() async* {
    Duration duration = expiredDate.difference(DateTime.now());
    int days = duration.inDays;
    int hours = duration.inHours - duration.inDays * 24;
    int minutes = duration.inMinutes - days * 24 * 60 - hours * 60;
    int seconds = duration.inSeconds -
        days * 24 * 60 * 60 -
        hours * 60 * 60 -
        minutes * 60;
    yield TimerState(
        days: days, hours: hours, minutes: minutes, seconds: seconds);
  }
}
