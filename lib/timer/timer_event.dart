import 'package:equatable/equatable.dart';

class TimerEvent extends Equatable {
  final EStatus status;
  @override
  List<Object> get props => [status];

  TimerEvent({this.status = EStatus.init});
}

enum EStatus { init, start, stop }
