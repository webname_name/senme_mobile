import 'dart:collection';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:senme/dto/product.dart';
import 'package:senme/dto/response_service.dart';
import 'package:senme/dto/token.dart';
import 'package:senme/dto/user_registration_form.dart';

class ApiService {
  static const String BASE_URL = "http://82.200.167.74:8098/senme";
  // static const String BASE_URL="http://192.168.0.107:8098/senme";

  Future<List<Product>> getGifts(int genderId) async {
    var dio = Dio();
    var params = new HashMap<String, dynamic>();
    params.putIfAbsent("typeId", () => 2);
    params.putIfAbsent("genderId", () => genderId);
    Response response =
        await dio.get("$BASE_URL/open/api/product", queryParameters: params);
    print("response.data: ${response.data}");
    List<dynamic> list = response.data;
    List<Product> products = new List();
    Product p;
    list.forEach((element) {
      p = Product.fromJson(element);
      products.add(p);
    });
    for (int i = 0; i < 100; i++) {
      products.add(p);
    }
    return products;
  }

  Future<ResponseService> registration(UserRegistrationForm form) async {
    var dio = Dio();
    var params = new HashMap<String, dynamic>();
    params.putIfAbsent("username", () => form.username);
    params.putIfAbsent("phoneNumber", () => form.phoneNumber);
    params.putIfAbsent("email", () => form.email);
    params.putIfAbsent("password", () => form.password);
    try {
      Response response = await dio.post("$BASE_URL/user/registration",
          data: form,
          options: Options(validateStatus: (status) => status < 500));
      print(
          "registration response.data>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>: ${response.data}");
      if (response.statusCode == HttpStatus.badRequest) {
        print(
            "registration failed with errors >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>: ${response.data}");
      }
      return ResponseService(
          response.data['message'] as String, response.statusCode);
    } on DioError catch (e) {
      if (e.type == DioErrorType.DEFAULT &&
          e.message.contains("SocketException")) {
        return ResponseService("Сервер не доступен", null);
      }
      return ResponseService(e.message, null);
    } on Exception catch (e) {
      print('registration exception is $e');
      return ResponseService(e.toString(), null);
    }
  }

  Future<ResponseService> getToken(String username, String password) async {
    var dio = Dio();
    var params = new HashMap<String, dynamic>();
    params.putIfAbsent("username", () => username);
    params.putIfAbsent("password", () => password);
    params.putIfAbsent("grant_type", () => "password");

    var headers = new HashMap<String, dynamic>();
    headers.putIfAbsent(
        "Authorization", () => "Basic Y2xpZW50SWRQYXNzd29yZDpzZWNyZXQ=");
    try {
      Response response = await dio.post("$BASE_URL/oauth/token",
          data: params,
          options: Options(
              headers: headers,
              contentType: "application/x-www-form-urlencoded",
              validateStatus: (status) => status < 500));
      print('getToken : ${response.data}');
      if (response.statusCode == HttpStatus.unauthorized) {
        return ResponseService(
            'Логин или пароль не верны', response.statusCode);
      } else if (response.statusCode == HttpStatus.ok) {
        return ResponseService(
            Token.fromJson(response.data), response.statusCode);
      }
    } on DioError catch (e) {
      if (e.type == DioErrorType.DEFAULT &&
          e.message.contains("SocketException")) {
        return ResponseService("Сервер не доступен", null);
      }
      return ResponseService(e.message, null);
    } on Exception catch (e) {
      print('getToken exception is $e');
      return ResponseService(e.toString(), null);
    }
  }
}
