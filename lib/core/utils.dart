import 'package:flutter/rendering.dart';
import 'package:url_launcher/url_launcher.dart';

class Utils {
  static bool isEmailValid(String value) {
    return RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(value);
  }

  static bool isPhoneNumberValid(String value) {
    return RegExp(r"\+\d \(\d\d\d\) \d{3}-\d{2}-\d{2}").hasMatch(value);
  }

  static Future<void> launchInBrowser(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
        forceSafariVC: false,
        forceWebView: false,
        // headers: <String, String>{'my_header_key': 'my_header_value'},
      );
    } else {
      throw 'Could not launch $url';
    }
  }

  static Size getTextSize(String text, TextStyle style) {
    final TextPainter tp = TextPainter(
        text: TextSpan(text: text, style: style),
        maxLines: 1,
        textDirection: TextDirection.ltr)
      ..layout(minWidth: 0, maxWidth: double.infinity);
    return tp.size;
  }
}
