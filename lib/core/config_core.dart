import 'dart:ui';

class ConfigCore {
  static final ConfigCore _instance = ConfigCore._privateConstructor();
  static ConfigCore get instance => _instance;
  ConfigCore._privateConstructor();

  static const String appName = 'Senme';

  static const colorPrimaryColor = Color(0xffC62121);
  static const greyColor = Color(0xff7E7E7E);

  // static const colorPrimary = Color(0xffEA152F);


  static const colorPrimary = Color(0xffE5E5E5);
  static const titleColor  = Color(0xff95A5BC);


}
