import 'package:equatable/equatable.dart';

class SignInOrSignUpState extends Equatable {
  final String username;
  final String password;
  final bool isAutoLogin;
  final bool isSignInPage;

  const SignInOrSignUpState(
      {this.username, this.password, this.isAutoLogin, this.isSignInPage});

  factory SignInOrSignUpState.initial() {
    return SignInOrSignUpState(
        username: '', password: '', isAutoLogin: false, isSignInPage: true);
  }

  SignInOrSignUpState copyWith(
      {String username, String password, bool isAutoLogin, bool isSignInPage}) {
    return SignInOrSignUpState(
        username: username ?? this.username,
        password: password ?? this.password,
        isAutoLogin: isAutoLogin ?? this.isAutoLogin,
        isSignInPage: isSignInPage ?? this.isSignInPage);
  }

  @override
  List<Object> get props => [username, password, isAutoLogin, isSignInPage];
}

class SignInStateSuccess extends SignInOrSignUpState {}
