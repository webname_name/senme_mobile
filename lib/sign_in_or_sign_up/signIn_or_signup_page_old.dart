import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:senme/core/config_core.dart';
import 'package:senme/profile/profile_event.dart';
import 'package:senme/sign_in/sign_in_screen_old.dart';
import 'package:senme/sign_in_or_sign_up/signIn_or_signup_state.dart';
import 'package:senme/sign_in_or_sign_up/sign_in_sign_up_bloc.dart';
import 'package:senme/sign_up/sign_up_screen_old.dart';

class SignInOrSignUpPageOld extends StatelessWidget {
  final String title;

  const SignInOrSignUpPageOld({Key key, this.title}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    SignInSignUpBloc signInSignUpBloc =
        BlocProvider.of<SignInSignUpBloc>(context);
    print('signInSignUpBloc: ${signInSignUpBloc.profileBloc}');
    return BlocListener<SignInSignUpBloc, SignInOrSignUpState>(
      listener: (context, state) {
        print('SignInOrSignUpPage listener: $state');
        if (state is SignInStateSuccess) {
          context
              .bloc<SignInSignUpBloc>()
              .profileBloc
              .add(ProfileEvent(EAction.LOGIN));
          Navigator.pop(context);
        }
      },
      child: BlocBuilder<SignInSignUpBloc, SignInOrSignUpState>(
          buildWhen: (prev, next) {
        if (next is SignInStateSuccess) {
          return false;
        } else {
          return true;
        }
      }, builder: (context, state) {
        print('SignInOrSignUpPage state.isSignInPage ${state.isSignInPage}');
        return MaterialApp(
          theme: ThemeData(
              // This is the theme of your application.
              //
              // Try running your application with "flutter run". You'll see the
              // application has a blue toolbar. Then, without quitting the app, try
              // changing the primarySwatch below to Colors.green and then invoke
              // "hot reload" (press "r" in the console where you ran "flutter run",
              // or simply save your changes to "hot reload" in a Flutter IDE).
              // Notice that the counter didn't reset back to zero; the application
              // is not restarted.
              primarySwatch: Colors.blue,
              primaryColor: Color(0xff2F80ED)),
          home: Scaffold(
            body: Stack(
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 24),
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Stack(
                          children: [
                            state.isSignInPage
                                ? SignInScreenOld(
                                    statusBarHeight:
                                        MediaQuery.of(context).padding.top,
                                    username: state.username,
                                    password: state.password,
                                    isAutoLogin: state.isAutoLogin)
                                : SignUpScreenOld(
                                    statusBarHeight:
                                        MediaQuery.of(context).padding.top),
                            Align(
                                alignment: Alignment.topLeft,
                                child: IconButton(
                                  icon: Icon(Icons.arrow_back_ios),
                                  color: ConfigCore.colorPrimaryColor,
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                )),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).padding.top,
                  color: ConfigCore.colorPrimaryColor,
                ),
              ],
            ),
          ),
        );
      }),
    );
  }
}
