import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:senme/profile/profile_bloc.dart';
import 'package:senme/sign_in_or_sign_up/signIn_or_signup_event.dart';
import 'package:senme/sign_in_or_sign_up/signIn_or_signup_state.dart';

class SignInSignUpBloc extends Bloc<SignInOrSignUpEvent, SignInOrSignUpState> {
  final ProfileBloc profileBloc;
  SignInSignUpBloc(SignInOrSignUpState initialState, this.profileBloc)
      : super(initialState);

  // @override
  // SignInOrSignUpState get initialState => SignInOrSignUpState.initial();

  @override
  Stream<SignInOrSignUpState> mapEventToState(
      SignInOrSignUpEvent event) async* {
    yield* eventController(event);
  }

  Stream<SignInOrSignUpState> eventController(
      SignInOrSignUpEvent event) async* {
    print('eventController: $event');
    if (event is ChangeViewMode) {
      yield state.copyWith(
          username: event.username,
          password: event.password,
          isAutoLogin: event.isAutoLogin,
          isSignInPage: !state.isSignInPage);
    }
    if (event is SignInEventSuccess) {
      profileBloc.authBloc.hasToken = true;
      yield SignInStateSuccess();
    }
  }
}
