import 'package:equatable/equatable.dart';

abstract class SignInOrSignUpEvent extends Equatable {
  const SignInOrSignUpEvent();
}

class ChangeViewMode extends SignInOrSignUpEvent {
  final String username;
  final String password;
  final bool isAutoLogin;
  ChangeViewMode(
      {this.username = '', this.password = '', this.isAutoLogin = false});
  @override
  List<Object> get props => [];
}

class SignInEventSuccess extends SignInOrSignUpEvent {
  @override
  List<Object> get props => [];
}
