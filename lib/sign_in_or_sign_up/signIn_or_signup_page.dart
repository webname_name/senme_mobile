import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:senme/core/config_core.dart';
import 'package:senme/profile/profile_event.dart';
import 'package:senme/sign_in/sign_in_screen.dart';
import 'package:senme/sign_in_or_sign_up/signIn_or_signup_state.dart';
import 'package:senme/sign_in_or_sign_up/sign_in_sign_up_bloc.dart';
import 'package:senme/sign_up/sign_up_screen.dart';

class SignInOrSignUpPage extends StatelessWidget {
  final String title;

  const SignInOrSignUpPage({Key key, this.title}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    SignInSignUpBloc signInSignUpBloc =
        BlocProvider.of<SignInSignUpBloc>(context);
    print('signInSignUpBloc: ${signInSignUpBloc.profileBloc}');
    return BlocListener<SignInSignUpBloc, SignInOrSignUpState>(
      listener: (context, state) {
        print('SignInOrSignUpPage listener: $state');
        if (state is SignInStateSuccess) {
          context
              .bloc<SignInSignUpBloc>()
              .profileBloc
              .add(ProfileEvent(EAction.LOGIN));
          Navigator.pop(context);
        }
      },
      child: BlocBuilder<SignInSignUpBloc, SignInOrSignUpState>(
          buildWhen: (prev, next) {
        if (next is SignInStateSuccess) {
          return false;
        } else {
          return true;
        }
      }, builder: (context, state) {
        print('SignInOrSignUpPage state.isSignInPage ${state.isSignInPage}');
        return Scaffold(
          appBar: AppBar(
            backgroundColor: ConfigCore.colorPrimary,
            title: Text(state.isSignInPage ? 'Вход' : 'Регистрация'),
            centerTitle: true,
          ),
          body: Stack(
            children: [
              SingleChildScrollView(
                child: state.isSignInPage
                    ? SignInScreen(
                        username: state.username,
                        password: state.password,
                        isAutoLogin: state.isAutoLogin)
                    : SignUpScreen(),
              )
            ],
          ),
        );
      }),
    );
  }
}
