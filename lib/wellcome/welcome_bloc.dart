import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:senme/wellcome/welcome_event.dart';
import 'package:senme/wellcome/welcome_state.dart';

class WelcomeBloc extends Bloc<WelcomeEvent, WelcomeState> {
  WelcomeBloc() : super(WelcomeStateInitial());
  @override
  Stream<WelcomeState> mapEventToState(WelcomeEvent event) async* {
    if (event is WelcomeEventInitial) {
      /*int delayTime=500;
      int currentValue=0;
      double progress=0;
      while(progress<1.0){
        await Future.delayed(Duration(milliseconds: 1),()=>currentValue+=1);
        progress=currentValue/delayTime;
        yield WelcomeStateProgressChanged(progress);
      }*/
      await Future.delayed(Duration(milliseconds: 2100), null);
      yield WelcomeStateInitialFinished();
    }
    else if(event is WelcomeEventInitialFinished){
      yield WelcomeStateInitialFinished();
    }
  }
}
