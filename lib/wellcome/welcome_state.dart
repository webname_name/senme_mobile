import 'package:equatable/equatable.dart';

abstract class WelcomeState extends Equatable {
  double progress = 0;
  @override
  List<Object> get props => [progress];
}

class WelcomeStateInitial extends WelcomeState {}

class WelcomeStateProgressChanged extends WelcomeState {
  double progress;

  WelcomeStateProgressChanged(this.progress);

  @override
  List<Object> get props => [progress];
}

class WelcomeStateInitialFinished extends WelcomeState {}
