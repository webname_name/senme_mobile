import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:senme/ui/images/pulsing_svg_picture.dart';
import 'package:senme/wellcome/welcome_bloc.dart';
import 'package:senme/wellcome/welcome_state.dart';

class WelcomePageOld extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<WelcomeBloc, WelcomeState>(
        builder: (context, state) => Scaffold(
              body: Stack(
                children: [
                  Align(
                    alignment: Alignment.center,
                    child: PulsingSvgPicture(
                      startWidth: 148.38,
                      endWidth: 160.0,
                      startHeight: 124.83,
                      endHeight: 140.0,
                      duration: Duration(milliseconds: 300),
                      assetSvgName: "images/heart.svg",
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: EdgeInsets.only(
                          bottom: MediaQuery.of(context).size.height * 0.111),
                      child: SvgPicture.asset(
                        "images/senme.svg",
                        width: 113.36,
                        height: 27.27,
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).padding.top,
                    color: Colors.white,
                  ),
                ],
              ),
              backgroundColor: Colors.white,
            ));
  }
}
