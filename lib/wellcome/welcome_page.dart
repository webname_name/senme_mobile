import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:senme/core/config_core.dart';
import 'package:senme/ui/widgets/welcome_animation.dart';
import 'package:senme/wellcome/welcome_bloc.dart';
import 'package:senme/wellcome/welcome_state.dart';

class WelcomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<WelcomeBloc, WelcomeState>(
        builder: (context, state) => Scaffold(
              body: WelcomeAnimationScreen(
                  screenWidth: MediaQuery.of(context).size.width,
                  screenHeight: MediaQuery.of(context).size.height),
              backgroundColor: ConfigCore.colorPrimary,
            ));
  }
}
