import 'package:equatable/equatable.dart';

class WelcomeEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class WelcomeEventInitial extends WelcomeEvent {}

class WelcomeEventInitialFinished extends WelcomeEvent {}
